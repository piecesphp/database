<?php

namespace Mappers;

use \PiecesPHP\Core\Database\EntityMapper;

/**
 * @property int $id
 * @property string $name
 * @property int|MainTableMapper $main_table_reference
 * @property string $sample_artificial_column_on_select (Solo lectura, es un alias)
 */
class SecondTableMapper extends EntityMapper
{

    const TABLE = 'second_table';
    protected $table = self::TABLE;

    protected $fields = [
        'id' => [
            'type' => 'int',
            'primary_key' => true,
        ],
        'name' => [
            'type' => 'varchar',
        ],
        'main_table_reference' => [
            'type' => 'int',
            'reference_table' => MainTableMapper::TABLE,
            'reference_field' => 'id',
            'human_readable_reference_field' => 'name',
            'has_many' => false,
            'mapper' => MainTableMapper::class,
        ],
        'sample_artificial_column_on_select' => [
            'representation_on_select_statement' => 'CONCAT(name, " - ", main_table_reference)',
            'meta' => true,
        ],
    ];

    public function __construct(int $value_compare = null, string $field_compare = 'primary_key', $options = null)
    {
        parent::__construct($value_compare, $field_compare, $options);
    }
}
