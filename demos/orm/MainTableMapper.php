<?php

namespace Mappers;

use PiecesPHP\Core\Database\ORM\Collections\FieldCollection;
use PiecesPHP\Core\Database\ORM\Fields\Field;
use PiecesPHP\Core\Database\ORM\Fields\SQLTypesEnum;
use PiecesPHP\Core\Database\ORM\ORM;

/**
 * @property int $id
 * @property string $name
 * @property string|SerializableClass $serialized_column
 */
class MainTableMapper extends ORM
{

    const TABLE = 'main_table';

    /**
     * @inheritDoc
     */
    protected $table = self::TABLE;

    /**
     * @inheritDoc
     */
    protected $onlySupportedTypes = true;

    public function __construct(int $value_compare = null, string $field_compare = 'primary_key', $options = null)
    {

        $id = (new Field('id', SQLTypesEnum::TYPE_INT))->sqlIsPrimaryKey(true)->sqlAutoIncrement(true);
        $name = (new Field('name', SQLTypesEnum::TYPE_VARCHAR))->sqlLength(255);
        $serializedColumn = (new Field('serialized_column', SQLTypesEnum::UTIL_TYPE_SERIALIZED))->sqlNullable(true);

        $this->fields = new FieldCollection([
            $id,
            $name,
            $serializedColumn,
        ]);

        parent::__construct($value_compare, $field_compare, $options);

    }
}
