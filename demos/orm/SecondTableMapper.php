<?php

namespace Mappers;

use PiecesPHP\Core\Database\ExtensibleORM;
use PiecesPHP\Core\Database\Meta\MetaProperty;
use PiecesPHP\Core\Database\ORM\Collections\FieldCollection;
use PiecesPHP\Core\Database\ORM\Fields\Field;
use PiecesPHP\Core\Database\ORM\Fields\ForeignKey;
use PiecesPHP\Core\Database\ORM\Fields\SQLTypesEnum;

/**
 * @property int $id
 * @property string $name
 * @property int|MainTableMapper $main_table_reference
 * @property string $sample_artificial_column_on_select (Solo lectura, es un alias)
 */
class SecondTableMapper extends ExtensibleORM
{

    const TABLE = 'second_table';
    protected $table = self::TABLE;

    public function __construct(int $value_compare = null, string $field_compare = 'primary_key', $options = null)
    {

        $id = new Field('id', SQLTypesEnum::TYPE_INT);
        $id->sqlIsPrimaryKey(true);
        $id->sqlAutoIncrement(true);

        $name = new Field('name', SQLTypesEnum::TYPE_VARCHAR);
        $name->sqlLength(255);

        $mainTableReference = new Field('main_table_reference', SQLTypesEnum::TYPE_INT);

        $foreign = new ForeignKey(MainTableMapper::TABLE, 'id', SQLTypesEnum::TYPE_INT, new MainTableMapper());

        $mainTableReference->foreignKey($foreign);

        $meta = new Field('meta', SQLTypesEnum::UTIL_TYPE_JSON, null);

        $meta2 = new Field('meta2', SQLTypesEnum::UTIL_TYPE_ARRAY, null);
        $meta2->sqlNullable(true);

        $this->addMetaProperty(new MetaProperty(MetaProperty::TYPE_DATE, 'now', true), 'startDate');

        $this->fields = new FieldCollection([
            $id,
            $name,
            $mainTableReference,
            $meta,
            $meta2,
        ]);

        parent::__construct($value_compare, $field_compare, $options);
    }
}
