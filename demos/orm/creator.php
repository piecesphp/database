<?php

use Mappers\MainTableMapper;
use Mappers\SecondTableMapper;
use PiecesPHP\Core\Database\Database;
use PiecesPHP\Core\Database\ORM\ORM;
use PiecesPHP\Core\Database\ORM\ORMSchemeCreator;

error_reporting(E_ALL);
ini_set('display_errors', true);

require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/MainTableMapper.php';
require_once __DIR__ . '/SecondTableMapper.php';
require_once __DIR__ . '/SerializableClass.php';

try {

    $database = Database::instanceMySQL('pcs_databases_orm', 'admin', '');
    ORM::setDatabase($database);

    $creators = ORMSchemeCreator::prepareGroup([
        new MainTableMapper(),
        new SecondTableMapper(),
    ]);

    //echo ORMSchemeCreator::getGroupSQL(true, true);exit;

    header('Content-Type: application/json');

    var_dump([
        'sql' => ORMSchemeCreator::getGroupSQL(true, true),
        'result' => ORMSchemeCreator::groupCreate(false, false),
    ]);

} catch (\Exception $e) {
    header('Content-Type: application/json');

    echo json_encode([
        $e->getMessage(),
        $e->getMessage(),
        $e->getLine(),
    ]);

    die;
}
