<?php

namespace Mappers;

use PiecesPHP\Core\Database\Database;
use PiecesPHP\Core\Database\Exceptions\DatabaseClassesExceptions;
use PiecesPHP\Core\Database\ORM\ORM;
use \Generic\SerializableClass;

error_reporting(E_ALL);
ini_set('display_errors', true);

/* set_error_handler(function ($int_error_type, $string_error_message, $string_error_file, $int_error_line) {
if (error_reporting() & $int_error_type) {
throw new \ErrorException($string_error_message, 0, $int_error_type, $string_error_file, $int_error_line);
}
return true;
}); */

require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/MainTableMapper.php';
require_once __DIR__ . '/SecondTableMapper.php';
require_once __DIR__ . '/SerializableClass.php';

try {

    $database = Database::instanceMySQL('pcs_databases_orm', 'admin', '');
    ORM::setDatabase($database);

    $mapperWithSerializedPropertyNew = new MainTableMapper(1);
    $mapperWithSerializedPropertyNew->name = rand(0, 50) . uniqid();

    $serializableObject = new SerializableClass(456);

    $mapperWithSerializedPropertyNew->serialized_column = $serializableObject;

    //var_dump($mapperWithSerializedPropertyNew->prepareModelForSave()->getCompiledSQL(true));exit;
    $mapperWithSerializedPropertyNew->update();

    $mapperWithReference = new SecondTableMapper(1); //Mapeador vacío
    $mapperWithReferenceEmpty = SecondTableMapper::getInstance(5, 'primary_key', false); //Mapeador con parámetro para llenar los valores
    $mapperWithReferenceNew = new SecondTableMapper();
    $mapperWithReferenceNewFromORM = new SecondTableMapper();

    $mapperWithReferenceNew->name = "La'bore v'olupt'ate nesciunt quisquam sapiente amet mol'estiae ipsam nemo sed amet corporis reprehenderit ex cum fugit cumque consequatur magna id";
    $mapperWithReferenceNew->main_table_reference = 1;
    $mapperWithReferenceNew->save();
    $mapperWithReferenceNew = new SecondTableMapper($mapperWithReferenceNew->getLastInsertID()); //Reinstanciado con el id

    $mapperWithReferenceNewFromORM->name = "La'bore v'olupt'ate nesciunt quisquam sapiente amet mol'estiae ipsam nemo sed amet corporis reprehenderit ex cum fugit cumque consequatur magna id";
    $mapperWithReferenceNewFromORM->main_table_reference = new MainTableMapper(1);
    $mapperWithReferenceNewFromORM->save();
    $mapperWithReferenceNewFromORM = new SecondTableMapper($mapperWithReferenceNewFromORM->getLastInsertID()); //Reinstanciado con el id

    $output = [
        'MainTableMapper Created' => $mapperWithSerializedPropertyNew->humanReadable(),
        'SecondTableMapper Empty' => $mapperWithReference->humanReadable(),
        'SecondTableMapper Getted' => $mapperWithReferenceEmpty->humanReadable(),
        'SecondTableMapper Created' => $mapperWithReferenceNew->humanReadable(),
        'SecondTableMapper Created From ORM on foreign' => $mapperWithReferenceNewFromORM->humanReadable(),
    ];

    highlight_string("<?php\n" . var_export($output, true) . ";\n?>");

} catch (DatabaseClassesExceptions $e) {
    header('Content-Type: application/json');

    echo json_encode([
        $e->getMessage(),
        $e->getLine(),
        $e->getCodeString(),
    ]);

    die;
}
