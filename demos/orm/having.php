<?php
error_reporting(E_ALL);
ini_set('display_errors', true);
use PiecesPHP\Core\Database\Database;
use PiecesPHP\Core\Database\ORM\ActiveRecord;
use PiecesPHP\Core\Database\ORM\Statements\Critery\WhereItem;
use PiecesPHP\Core\Database\ORM\Statements\Critery\WhereItemGroup;
use PiecesPHP\Core\Database\ORM\Statements\WhereSegment;

require_once __DIR__ . '/../../vendor/autoload.php';

try {

    $results = [];
    $table_name = 'second_table';
    $joined_table = 'main_table';

    $prettyWhere =
    (new WhereSegment())
        ->addGroup(
            new WhereItemGroup(
                [
                    new WhereItem("{$table_name}.id", WhereItem::LESS_THAN_OPERATOR, 6, WhereItem::OR_OPERATOR),
                    new WhereItem("{$table_name}.id", WhereItem::EQUAL_OPERATOR, 20, WhereItem::OR_OPERATOR),
                    new WhereItem("{$table_name}.id", WhereItem::EQUAL_OPERATOR, 25, WhereItem::OR_OPERATOR),
                ]
            )
        )
        ->addGroup(
            new WhereItemGroup(
                [
                    new WhereItem("LENGTH({$table_name}.name)", WhereItem::GREATER_THAN_OPERATOR, 3, WhereItem::OR_OPERATOR),
                    new WhereItem(45, WhereItem::EQUAL_OPERATOR, "33+45"),
                ]
            )
        );

    $simpleWhere = [
        "{$table_name}.id" => [
            '>' => 0,
        ],
    ];
    $complexWhere = [
        "{$table_name}.id" => [
            'multiple' => [
                [
                    'and_or' => 'OR',
                    '<' => 6,
                ],
                [
                    'and_or' => 'OR',
                    '=' => 20,
                ],
                [
                    'and_or' => 'OR',
                    '=' => 25,
                ],
            ],
        ],
        "LENGTH({$table_name}.name)" => [
            'multiple' => [
                [
                    'and_or' => 'OR',
                    '>' => 3,
                ],
            ],
        ],
        45 => "33 + 45",
    ];

    $whereSegment = $simpleWhere;

    switch ('pretty') {
        case 'complex':
            $whereSegment = $complexWhere;
            break;
        case 'pretty':
            $whereSegment = $prettyWhere;
            break;
    }

    //var_dump($prettyWhere->getReplacementValues());exit;

    for ($i = 0; $i < 1; $i++) {

        $database = Database::instanceMySQL('pcs_databases_orm', 'admin', '');
        $model = new ActiveRecord($database);

        $model->setTable($table_name);

        $model->select([
            "{$table_name}.id AS {$table_name}_id",
            "{$table_name}.name AS {$table_name}_name",
            "{$joined_table}.id AS {$joined_table}_id",
            "{$joined_table}.name AS {$joined_table}_name",
            "{$joined_table}.serialized_column AS {$joined_table}_serialized_column",
        ])
            ->where($whereSegment)
            ->innerJoin(
                $joined_table,
                [
                    "{$joined_table}.id" => "{$table_name}.main_table_reference",
                ]
            )
            ->having([
                "{$table_name}_name" => "Werner",
            ]);

        $compiledSQL = '';
        $compiledSQL = $model->getCompiledSQL(false);
        $compiledSQLReplaced = $model->getCompiledSQL(true);
        $model->execute();
        $result = $model->result();

        $results[] = [
            'compiledSQL' => $compiledSQL,
            'compiledSQLReplaced' => $compiledSQLReplaced,
            'lastSQLExecuted' => $model->getLastSQLExecuted(),
            'result' => $result,
        ];
    }

    highlight_string("<?php\n" . var_export($results, true) . ";\n?>");

} catch (\PDOException $e) {
    var_dump(
        $e->getMessage(),
        $e->getCode(),
        $e->getFile(),
        $e->getLine(),
    );
}
