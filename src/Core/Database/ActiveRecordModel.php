<?php

/**
 * ActiveRecordModel.php
 */
namespace PiecesPHP\Core\Database;

use PiecesPHP\Core\Database\Exceptions\OptionPatternMismatchException;
use PiecesPHP\Core\Database\Exceptions\RequiredOptionMissedException;
use PiecesPHP\Core\Database\ORM\ActiveRecord;
use TypeError;
use \Exception;

/**
 * ActiveRecordModel - Implementación básica de modelo ActiveRecord.
 *
 * @package     PiecesPHP\Core\Database
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2018
 */
class ActiveRecordModel extends ActiveRecord
{
    /**
     * @var string
     */
    protected $table = '';

    /**
     * @var string
     */
    protected $tablePrefix = '';

    /** @var Database[] $db Instancias de PiecesPHP\Core\Database */
    protected static $db = [];

    /**
     * @var array Opciones por defecto
     */
    protected static $defaultValueOptions = [
        'driver' => 'mysql',
        'database' => '',
        'host' => 'localhost',
        'user' => 'root',
        'password' => '',
        'charset' => 'utf8',
        'config' => true,
        'table' => 'NOT_DEFINED_TABLE',
    ];

    /**
     * @var array Opciones de configuración de la base de datos (de la instancia)
     */
    protected $instanceOptionsDb = [];

    /**
     * @var array Opciones de configuración de la base de datos (generales)
     */
    protected static $optionsDb = [];

    //Acciones
    const INSERT = self::ACTION_UPDATE;
    const SELECT = self::ACTION_SELECT;
    const UPDATE = self::ACTION_UPDATE;
    const DELETE = self::ACTION_DELETE;

    /**
     * @param array $options Las opciones de configuración
     *
     * Opciones aceptadas:
     * - driver (string)
     * - database (string)
     * - host (string)
     * - user (string)
     * - password (string)
     * - charset (string)
     * - config (bool)
     * - table (string)
     * @throws RequiredOptionMissedException|OptionPatternMismatchException
     */
    public function __construct(array $options = [])
    {
        if (!array_key_exists('config', $options)) {
            $options['config'] = static::$defaultValueOptions['config'];
        } elseif (!is_bool($options['config'])) {
            $options['config'] = true;
        }

        $config = $options['config'];

        if ($config) {
            $this->configDb($options);
            parent::__construct(self::$db[$this->instanceOptionsDb['key']], $this->getTable());
        } else {

            if (is_string($this->tablePrefix)) {
                $this->table = trim($this->tablePrefix) . trim($this->table);
            }

            if (is_array($this->fields) && is_string($this->table)) {
                $this->configFields();
            }

        }
    }

    /**
     * Establece la propiedad db.
     * @param Database $database
     * @return static
     */
    public function setDatabase(Database $database)
    {
        $key = self::generateKeyDb($database->getHost(), $database->getDatabaseName());
        $this->instanceOptionsDb = [
            'driver' => $database->getDriver(),
            'database' => $database->getDatabaseName(),
            'host' => $database->getHost(),
            'user' => $database->getUsername(),
            'password' => $database->getPassword(),
            'charset' => 'utf8',
            'table' => $this->table,
            'key' => $key,
        ];
        self::$db[$key] = $database;
        return $this;
    }

    /**
     * Establece opciones de la configuración de la base de datos del objeto.
     * @param array $options Las opciones de configuración
     *
     * Opciones aceptadas:
     * - driver (string)
     * - database (string)
     * - host (string)
     * - user (string)
     * - password (string)
     * - charset (string)
     * - config (bool)
     * - table (string)
     * @return void
     * @throws RequiredOptionMissedException|OptionPatternMismatchException
     */
    public function configDb(array $options)
    {
        $options = static::_validateOptions($options);
        $driver = $options['driver'];
        $database = $options['database'];
        $host = $options['host'];
        $user = $options['user'];
        $password = $options['password'];
        $charset = $options['charset'];
        $table = $options['table'];
        $key = $options['key'];

        $existsDatabaseConnection = array_key_exists($key, self::$db);
        $existsDatabaseConnection = $existsDatabaseConnection ? self::$db[$key] instanceof Database : false;
        self::$db[$key] = $existsDatabaseConnection ? self::$db[$key] : Database::instance($database, $user, $password, $host, $driver, $charset);

        if ($this->table === null) {
            $this->setTable($table);
        } else {
            if (is_string($this->tablePrefix)) {
                $this->table = trim($this->tablePrefix) . trim($this->table);
            }
        }

        $this->instanceOptionsDb = $options;
    }

    /**
     * Asigna el prefijo tabla del modelo.
     * @param string $tablePrefix Prefijo de la tabla
     * @return static
     */
    public function setPrefixTable(string $tablePrefix)
    {
        $last_prefix = $this->tablePrefix;
        $this->tablePrefix = trim($tablePrefix);

        if (is_string($this->table)) {

            if (is_string($this->tablePrefix)) {

                $this->table = str_replace($last_prefix, '', $this->table);
            }

            $this->setTable($this->table);
        }

        return $this;
    }

    /**
     * Asigna el nombre del tabla del modelo.
     * @param string $table Nombre de la tabla
     * @return static
     */
    public function setTable(string $table)
    {
        if (is_string($this->tablePrefix) && mb_strlen($this->tablePrefix) > 0) {
            $this->table = trim($this->tablePrefix) . trim($table);
        } else {
            $this->table = trim($table);
        }

        return $this;
    }

    /**
     * Obtiene y configura los nombres de las columnas
     * @return array
     */
    public function configFields()
    {
        $key = $this->instanceOptionsDb['key'];
        $pdoStatement = self::$db[$key]->query("DESCRIBE $this->table");
        $result = $pdoStatement !== false ? $pdoStatement->fetchAll(Database::FETCH_OBJ) : [];

        $this->fields = [];

        foreach ($result as $field_data) {
            $field = $field_data->Field;
            $this->fields[] = $field;
        }

        return $result;
    }

    /**
     * Devuelve el prefijo de la tabla
     *
     * @return string
     */
    public function getPrefixTable()
    {
        return $this->tablePrefix;
    }

    /**
     * Devuelve la información de la tabla
     *
     * @return array
     */
    public function getTableInformation()
    {
        $key = $this->instanceOptionsDb['key'];
        $pdoStatement = self::$db[$key]->query("DESCRIBE $this->table");
        $result = $pdoStatement !== false ? $pdoStatement->fetchAll(Database::FETCH_OBJ) : null;

        return is_array($result) ? $result : [];
    }

    /**
     * Devuelve los campos de la tabla
     *
     * @return array
     */
    public function getFields()
    {
        if ($this->fields === null) {
            $this->configFields();
        }
        return $this->fields;
    }

    /**
     * Devuelve la consulta preparada
     *
     * @return \PDOStatement|string Devuelve el string del error en caso de una excepción.
     */
    public function getPrepareStatement()
    {
        try {
            return $this->prepare(trim($this->sql));
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param mixed $host
     * @param mixed $databaseName
     * @return string
     */
    public static function generateKeyDb($host, $databaseName)
    {
        $key = [
            'host' => $host,
            'database' => $databaseName,
        ];
        $key = json_encode($key);
        $key = is_string($key) ? $key : 'secret';
        $key = base64_encode($key);
        return $key;
    }

    /**
     * @param string $key1
     * @param string $key2
     * @return bool
     */
    public static function compareDbKeys(string $key1, string $key2)
    {

        $key1 = base64_decode($key1);
        $key1 = json_decode($key1);

        $key2 = base64_decode($key2);
        $key2 = json_decode($key2);

        if (!is_array($key1) || !is_array($key2)) {
            return false;
        }

        $key1['host'] = !isset($key1['host']) ? null : $key1['host'];
        $key1['database'] = !isset($key1['database']) ? null : $key1['database'];

        $key2['host'] = !isset($key2['host']) ? null : $key2['host'];
        $key2['database'] = !isset($key2['database']) ? null : $key2['database'];

        if ($key1['host'] === null || $key2['host'] === null) {
            return false;
        }

        if ($key1['database'] === null || $key2['database'] === null) {
            return false;
        }

        return $key1['host'] == $key2['host'] && $key1['database'] == $key2['database'];
    }

    /**
     * Establece la propiedad db.
     * @param Database $database Instancia de Database
     * @param string $databaseName Nombre de la base de datos
     * @param string $host Sevidor
     * @return void
     */
    public static function setDb(Database $database, string $databaseName, string $host = 'localhost')
    {
        $key = self::generateKeyDb($host, $databaseName);
        self::$db[$key] = $database;
    }

    /**
     * Establece opciones de la configuración de la base de datos
     * Nota: las opciones de la clase, no del objeto que puede ser diferente
     * si se definen en el constructor.
     *
     * @param array $options Las opciones de configuración (ver la constante de clase DEFAULT_OPTIONS_COMMENTS)
     *
     * @return void
     * @throws RequiredOptionMissedException|OptionPatternMismatchException
     */
    public static function setOptionsDb(array $options)
    {
        static::$optionsDb = static::_validateOptions($options);
    }

    /**
     * Devuelve el valor de la propiedad db.
     * @param string $databaseName Nombre de la base de datos
     * @param string $host Servidor
     * @return Database|null
     */
    public static function getDb(string $databaseName, string $host = 'localhost')
    {
        $key = self::generateKeyDb($host, $databaseName);
        return isset(self::$db[$key]) ? self::$db[$key] : null;
    }

    /**
     * Devuelve las opciones de la configuración de la base de datos
     * Nota: las opciones de la clase, no del objeto que puede ser diferente
     * si se definen en el constructor.
     *
     * @return array
     */
    public static function getOptionsDb()
    {
        if (count(static::$optionsDb) > 0) {
            return static::$optionsDb;
        } else {
            return static::$defaultValueOptions;
        }
    }

    /**
     * @param array $options Las opciones de configuración (ver la constante de clase DEFAULT_OPTIONS_COMMENTS)
     * @return bool
     */
    public static function validateOptions(array $options)
    {
        try {
            static::_validateOptions($options);
            return true;
        } catch (RequiredOptionMissedException | OptionPatternMismatchException $e) {
            return false;
        }
    }

    /**
     * @param array $options Las opciones de configuración (ver la constante de clase DEFAULT_OPTIONS_COMMENTS)
     * @return array Las opciones
     * @throws RequiredOptionMissedException|OptionPatternMismatchException
     */
    protected static function _validateOptions(array $options)
    {

        $defaultOptions = static::$defaultValueOptions;

        $isValidString = function ($e) {
            return is_string($e);
        };

        $isBool = function ($e) {
            return $e === true || $e === false;
        };

        $patternsOptions = [
            'driver' => [
                'validate' => $isValidString,
                'type' => 'string',
            ],
            'database' => [
                'validate' => $isValidString,
                'type' => 'string',
            ],
            'host' => [
                'validate' => $isValidString,
                'type' => 'string',
            ],
            'user' => [
                'validate' => $isValidString,
                'type' => 'string',
            ],
            'password' => [
                'validate' => $isValidString,
                'type' => 'string',
            ],
            'charset' => [
                'validate' => $isValidString,
                'type' => 'string',
            ],
            'config' => [
                'validate' => $isBool,
                'type' => 'bool',
            ],
            'table' => [
                'validate' => $isValidString,
                'type' => 'string',
            ],
        ];

        foreach ($patternsOptions as $option => $pattern) {
            if (!array_key_exists($option, $options) || is_null($options[$option])) {
                if (array_key_exists($option, $defaultOptions)) {
                    $options[$option] = $defaultOptions[$option];
                }
            }
        }

        foreach ($patternsOptions as $option => $validator) {

            $type = $validator['type'];

            if (!array_key_exists($option, $options)) {
                throw new RequiredOptionMissedException($option);
            }

            if (!($validator['validate'])($options[$option])) {
                throw new TypeError("{$option} debe ser un {$type}");
            }

        }

        $options['key'] = self::generateKeyDb($options['host'], $options['database']);

        return $options;
    }

    /**
     * Conversión a string
     * @return string
     */
    public function __toString()
    {
        return $this->jsonSerialize();
    }

    /**
     * Conversión a string
     * @return string
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
        $data = [];

        $data['db'] = self::$db;

        $data['table'] = $this->table;

        $data['tablePrefix'] = $this->tablePrefix;

        $data['fields'] = $this->fields;

        $data['typeResultAssoc'] = $this->typeResultAssoc;

        $data['prepareStatement'] = $this->prepareStatement;

        $data['sql'] = trim($this->sql);
        $data['compiledSQL'] = $this->getCompiledSQL();
        $data['lastSQLExecuted'] = trim($this->lastSQLExecuted);

        $data['select'] = "{$this->selectQuery}";
        $data['insert'] = !is_array($this->insertSegment) ? "{$this->insertSegment}" : json_encode($this->insertSegment);
        $data['update'] = !is_array($this->updateSegment) ? "{$this->updateSegment}" : json_encode($this->updateSegment);
        $data['join'] = "{$this->joinSegment}";
        $data['where'] = "{$this->whereSegment}";
        $data['groupBy'] = "{$this->groupByString}";
        $data['having'] = "{$this->havingSegment}";
        $data['orderBy'] = "{$this->orderByString}";

        $data['currentAction'] = $this->currentAction;

        $data['replacesValues'] = $this->getReplaceValues();
        $data['replacesValuesWhereAndHavin'] = $this->getReplaceWhereAndHavingValues();

        $data['resultSet'] = $this->resultSet;

        $data = json_encode($data);
        $data = is_string($data) ? $data : '{}';

        return $data;
    }
}
