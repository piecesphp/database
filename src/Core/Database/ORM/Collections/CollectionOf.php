<?php

/**
 * CollectionOf.php
 */
namespace PiecesPHP\Core\Database\ORM\Collections;

use Exception;
use PiecesPHP\Core\Database\Util\ArrayObjectExtend;

/**
 * CollectionOf
 *
 * @package     PiecesPHP\Core\Database\ORM\Collections
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2020
 */
class CollectionOf extends ArrayObjectExtend
{
    /**
     * Constante para establecer el tipo como integer
     *
     * @var string
     */
    const TYPE_INTEGER = 'integer';

    /**
     * Constante para establecer el tipo como double
     *
     * @var string
     */
    const TYPE_DECIMAL = 'double';

    /**
     * Constante para establecer el tipo como string
     *
     * @var string
     */
    const TYPE_STRING = 'string';

    /**
     * Constante para establecer el tipo como object
     *
     * Este valor establece que los elementos de entrada sean objetos y requiere
     * que se defina en el constructuctor el nombre cualificado de la clase que adminitirá
     *
     * @var string
     */
    const TYPE_OBJECT = 'object';

    /**
     * Constante para establecer el tipo como self
     *
     * Este valor establece que los elementos de entrada sean instancias de si mismo
     *
     * @var string
     */
    const TYPE_SELF = 'self';

    /**
     * @var string
     */
    protected $type = 'string';

    /**
     * @var string
     */
    protected $qualifiedName = null;

    /**
     * @param mixed $input Elemento(s) de entrada, puede ser un elemento del tipo admitido o un array
     * que contenga elementos únicamente del tipo admitido.
     * @param string $type Tipo de elemento aceptado. Están definidos en la constanstes de clase.
     * Si el TYPE_OBJECT entonces debe ser suministrado el nombre cualificado del objeto.
     * @param string $qualifiedName Nombre cualificado del objeto. Puede ser obtenido
     * con la nomenclatura NombreDelObjeto::class
     * @throws Exception
     */
    public function __construct($input = [], string $type = self::TYPE_STRING, string $qualifiedName = null)
    {

        $this->validateType($type);

        $this->type = $type;

        if ($this->type === self::TYPE_OBJECT) {
            if ($qualifiedName !== null) {
                $this->qualifiedName = $qualifiedName;
            } else {
                throw new Exception('El nombre no es válido.');
            }
        }

        if (!is_array($input)) {
            $input = [$input];
        }

        foreach ($input as $item) {
            $this->validateInput($item);
        }

        parent::__construct($input);
    }

    /**
     * @param mixed $input Elemento del tipo admitido
     * @throws Exception
     */
    public function append($input)
    {
        $this->validateInput($input);

        parent::append($input);
    }

    /**
     * Intercambia el array por otro
     *
     * @param mixed $input Elemento(s) de entrada, puede ser un elemento del tipo admitido o un array
     * que contenga elementos únicamente del tipo admitido.
     * @return array El array anterior
     * @throws Exception
     */
    public function exchangeArray($input)
    {
        if (is_array($input)) {
            foreach ($input as $item) {
                $this->validateInput($item);
            }
        } else {
            $this->validateInput($input);
        }
        return parent::exchangeArray($input);
    }

    /**
     * @param mixed $index Índice a ser establecido
     * @param mixed $value Elemento de entrada
     * @return void
     * @throws Exception
     */
    public function offsetSet($index, $value)
    {
        $this->validateInput($value);

        parent::offsetSet($index, $value);
    }

    /**
     * @param string $type
     * @return void
     * @throws Exception
     */
    protected function validateType(string $type)
    {
        $valid = false;

        switch ($type) {
            case self::TYPE_INTEGER:
            case self::TYPE_DECIMAL:
            case self::TYPE_STRING:
            case self::TYPE_OBJECT:
            case self::TYPE_SELF:
                $valid = true;
                break;
            default:
                $valid = false;
        }

        if (!$valid) {
            throw new Exception('El tipo no es permitido.');
        }
    }

    /**
     * @param mixed $input
     * @return void
     * @throws Exception
     */
    protected function validateInput($input)
    {
        $valid = true;

        switch ($this->type) {
            case self::TYPE_INTEGER:
                $valid = is_integer($input);
                break;
            case self::TYPE_DECIMAL:
                $valid = is_float($input);
                break;
            case self::TYPE_STRING:
                $valid = is_string($input);
                break;
            case self::TYPE_OBJECT:
                $valid = $input instanceof $this->qualifiedName;
                break;
            case self::TYPE_SELF:
                $class_name = get_class($this);
                $valid = $input instanceof $class_name;
                break;
            default:
                $valid = false;
                break;
        }

        if (!$valid) {
            throw new Exception('El tipo no es permitido.');
        }
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getQualifiedName()
    {
        return $this->qualifiedName;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $elements = $this->getArrayCopy();

        if ($this->type === self::TYPE_OBJECT) {
            $elements = array_map(function ($e) {
                return serialize($e);
            }, $elements);
        }

        $json = json_encode($elements);
        return is_string($json) ? $json : '{}';
    }
}
