<?php
/**
 * FieldCollection.php
 */
namespace PiecesPHP\Core\Database\ORM\Collections;

use PiecesPHP\Core\Database\ORM\Fields\Field;

/**
 * FieldCollection.
 *
 * @package     PiecesPHP\Core\Database\ORM\Collections
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2020
 */
class FieldCollection extends CollectionOf
{

    /**
     * @param Field[] $input
     */
    public function __construct($input = [])
    {
        parent::__construct($input, self::TYPE_OBJECT, Field::class);
    }

    /**
     * @param Field $field
     * @throws \Exception
     */
    public function append($field)
    {
        parent::append($field);
    }

    /**
     * @param string $name
     * @return Field|null
     */
    public function getByName(string $name)
    {
        $field = null;
        foreach ($this as $index => $field) {
            if ($field->getName() == $name) {
                return $field;
            }
        }
        return null;
    }

    /**
     * @param string $name
     * @return int|null
     */
    public function getPositionField(string $name)
    {
        $field = null;
        foreach ($this as $index => $field) {
            if ($field->getName() == $name) {
                return $index;
            }
        }
        return null;
    }

    /**
     * @return string[]
     */
    public function getNames()
    {
        $names = [];
        $fields = $this->getArrayCopy();
        foreach ($fields as $field) {
            $names[] = $field->getName();
        }
        return $names;
    }

}
