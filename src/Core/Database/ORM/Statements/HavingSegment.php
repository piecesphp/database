<?php
/**
 * HavingSegment.php
 */
namespace PiecesPHP\Core\Database\ORM\Statements;

use PiecesPHP\Core\Database\ORM\Statements\Critery\HavingItem;

/**
 * HavingSegment.
 *
 * @package     PiecesPHP\Core\Database\ORM\Statements
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2020
 */
class HavingSegment
{
    /**
     * @var HavingItem[]
     */
    protected $criteria = [];

    /**
     * @param HavingItem[] $criteria
     */
    public function __construct(array $criteria = [])
    {
        $this->setCriteria($criteria);

    }

    /**
     * @param HavingItem $critery
     * @return static
     */
    public function addCritery(HavingItem $critery)
    {
        $this->criteria[] = $critery;
        return $this;
    }

    /**
     * @return int
     */
    public function countCriteria()
    {
        return count($this->criteria);
    }

    /**
     * @param HavingItem[] $criteria
     * @return static
     */
    public function setCriteria(array $criteria)
    {
        $this->criteria = [];
        foreach ($criteria as $critery) {
            $this->addCritery($critery);
        }
        return $this;
    }

    /**
     * @return array<string,int>|array<string,float>|array<string,string>|array<string,bool>|array<string,null>
     */
    public function getReplacementValues()
    {
        $replacementValues = [];

        foreach ($this->criteria as $critery) {
            $criteryReplacement = $critery->getReplacementValues();
            if (isset($criteryReplacement['alias']) && isset($criteryReplacement['value'])) {
                $replacementValues[$criteryReplacement['alias']] = $criteryReplacement['value'];
            }
        }

        return $replacementValues;
    }

    /**
     * @return string
     */
    public function toString()
    {
        $criteria = [];
        $countCriteria = $this->countCriteria();
        $lasIndex = array_key_last($this->criteria);

        foreach ($this->criteria as $index => $critery) {

            $isLast = $index === $lasIndex;

            if ($countCriteria === 1 || $isLast) {
                $criteria[] = $critery->toString(false);
            } else {
                $criteria[] = "{$critery}";
            }

        }

        $criteria = implode(" ", $criteria);

        return "HAVING ({$criteria})";
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->toString();
    }

}
