<?php
/**
 * SelectQuerySegment.php
 */
namespace PiecesPHP\Core\Database\ORM\Statements;

/**
 * SelectQuerySegment.
 *
 * @package     PiecesPHP\Core\Database\ORM\Statements
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2020
 */
class SelectQuerySegment
{
    /**
     * @var string
     */
    protected $table = '';
    /**
     * @var string[]
     */
    protected $fields = [];
    /**
     * @var callable
     * @return string
     */
    protected $scapeReservedWordsStrategy = null;

    /**
     * @param string[] $fields
     * @param string $table
     */
    public function __construct(array $fields = null, string $table = '')
    {

        $fields = $fields === null ? [] : $fields;
        $this->table = $table;

        $this->scapeReservedWordsStrategy = function ($word) {

            $word = trim($word);

            if ($word !== '*') {
                $word = "`{$word}`";
            }

            return $word;
        };

        $this->setFields($fields);

    }

    /**
     * Función usada para escapar los valores individuales que representan los campos
     *
     * @param callable $function Debe recibir un string y devolver un string que contenga el recibido
     * @return static
     */
    public function setScapeReservedWordStrategy(callable $function)
    {

        $testString = 'testing';
        $testOutput = ($function)($testString);

        if (is_string($testOutput) && strpos($testOutput, $testString) !== false) {
            $this->scapeReservedWordsStrategy = $function;
        }

        return $this;

    }

    /**
     * @param string $field
     * @param boolean $scape
     * @return static
     */
    public function addField(string $field, bool $scape = false)
    {
        $this->fields[] = $scape ? ($this->scapeReservedWordsStrategy)($field) : $field;
        return $this;
    }

    /**
     * @param array $fields
     * @param bool $scape
     * @return static
     */
    public function setFields(array $fields, bool $scape = false)
    {
        $this->fields = [];
        foreach ($fields as $field) {
            $this->addField($field, $scape);
        }
        return $this;
    }

    /**
     * @param string $table
     * @return static
     */
    public function setTable(string $table)
    {
        $this->table = $table;
        return $this;
    }

    /**
     * @return string
     */
    public function toString()
    {
        $fields = $this->fields;
        if (count($fields) > 0) {
            $fields = implode(", ", $fields);
        } else {
            $fields = "*";
        }

        return "SELECT {$fields} FROM {$this->table}";
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->toString();
    }

}
