<?php
/**
 * WhereItem.php
 */
namespace PiecesPHP\Core\Database\ORM\Statements\Critery;

use Exception;

/**
 * WhereItem.
 *
 * @package     PiecesPHP\Core\Database\ORM\Statements\Critery
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2020
 */
class WhereItem
{
    const EQUAL_OPERATOR = '=';
    const GREATER_THAN_OPERATOR = '>';
    const LESS_THAN_OPERATOR = '<';
    const GREATER_OR_EQUAL_OPERATOR = '>=';
    const LESS_OR_EQUAL_OPERATOR = '<=';
    const NOT_EQUAL_OPERATOR = '!=';
    const IS_NULL_OPERATOR = 'IS NULL';
    const IS_NOT_NULL_OPERATOR = 'IS NOT NULL';
    const LIKE_OPERATOR = 'LIKE';
    const NOT_LIKE_OPERATOR = 'NOT LIKE';
    const IN_OPERATOR = 'IN';
    const NOT_IN_OPERATOR = 'NOT IN';
    const FIND_IN_SET_OPERATOR = 'FIND_IN_SET({SEARCH}, {VALUES})';

    const AND_OPERATOR = 'AND';
    const OR_OPERATOR = 'OR';

    const REPLACEMENT_VALUE_ON_RIGHT_WRAP_FUNCTION = '{%VALUE%}';

    /**
     * @var string
     */
    protected $leftMember = '';
    /**
     * @var string
     */
    protected $operator = '';
    /**
     * @var int|float|string|bool|null
     */
    protected $rightMember = '';
    /**
     * @var string
     */
    protected $afterOperator = '';
    /**
     * @var ?string
     */
    protected $rightWrapFunction = '';
    /**
     * @var array
     * Opciones:
     * - alias (string)
     * - value (int|float|string|bool|null)
     */
    protected $replacementValues = [];

    /**
     * @param string $leftMember
     * @param string $operator
     * @param int|float|string|bool|null $rightMember
     * @param string $afterOperator
     * @param string $rightWrapFunction Función que encerrará el extremo derecho, con plantilla {%VALUE%} para el valor. Ejemplo: DATE_FORMAT({%VALUE%}, "%Y")
     */
    public function __construct(string $leftMember, string $operator = self::EQUAL_OPERATOR, $rightMember = '', string $afterOperator = '', string $rightWrapFunction = null)
    {
        $this->setLeftMember($leftMember);
        $this->setOperator($operator);
        $this->setRightMember($rightMember);
        $this->setAfterOperator($afterOperator);
        $this->setRightWrapFunction($rightWrapFunction);

        $base = str_replace([' ', '`', '"', "'"], '', $this->leftMember);
        $base = preg_replace('|\&.*\;|', '', htmlentities($base));
        $replacementParam = preg_replace("/[.|\(|\)|-|,]/", '', ':WH' . uniqid() . "_{$base}");
        $replacementParam = mb_strtoupper($replacementParam);

        $notAliasOperators = [
            self::IS_NULL_OPERATOR,
            self::IS_NOT_NULL_OPERATOR,
            self::IN_OPERATOR,
            self::NOT_IN_OPERATOR,
            self::FIND_IN_SET_OPERATOR,
        ];
        if (!in_array($this->operator, $notAliasOperators)) {
            $this->replacementValues['alias'] = $replacementParam;
            $this->replacementValues['value'] = $this->rightMember;
        }

    }

    /**
     * @param string $value
     * @return WhereItem
     */
    public function setLeftMember(string $value)
    {
        $this->leftMember = trim($value);
        return $this;
    }

    /**
     * @param string $value
     * @return WhereItem
     */
    public function setOperator(string $value)
    {
        $this->operator = mb_strtoupper(trim($value));
        return $this;
    }

    /**
     * @param int|float|string|bool|null $value
     * @return WhereItem
     */
    public function setRightMember($value)
    {
        if (is_scalar($value) || $value === null) {
            $this->rightMember = is_string($value) ? trim($value) : $value;
        } else {
            throw new Exception('$value es inválido.');
        }
        return $this;
    }

    /**
     * @param string $value
     * @return WhereItem
     */
    public function setAfterOperator(string $value)
    {
        $this->afterOperator = mb_strtoupper(trim($value));
        $this->afterOperator = mb_strlen($this->afterOperator) > 0 ? $this->afterOperator : self::AND_OPERATOR;
        return $this;
    }

    /**
     * @param string $value
     * @return WhereItem
     */
    public function setRightWrapFunction(?string $value)
    {
        $this->rightWrapFunction = is_string($value) && mb_strpos($value, self::REPLACEMENT_VALUE_ON_RIGHT_WRAP_FUNCTION) ? mb_strtoupper(trim($value)) : '';
        $this->rightWrapFunction = mb_strlen($this->rightWrapFunction) > 0 ? $this->rightWrapFunction : null;
        return $this;
    }

    /**
     * @return string
     */
    public function getLeftMember()
    {
        return $this->leftMember;
    }

    /**
     * @return string
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * @return int|float|string|bool|null
     */
    public function getRightMember()
    {
        return $this->rightMember;
    }

    /**
     * @return string
     */
    public function getAfterOperator()
    {
        return $this->afterOperator;
    }

    /**
     * @return ?string
     */
    public function getRightWrapFunction()
    {
        return $this->rightWrapFunction;
    }

    /**
     * @return array
     * Opciones:
     * - alias (string)
     * - value (int|float|string|bool|null)
     */
    public function getReplacementValues()
    {
        return $this->replacementValues;
    }

    /**
     * @param bool $withAfterOperator
     * @return string
     */
    public function toString(bool $withAfterOperator = true)
    {
        if ($this->operator == self::IS_NULL_OPERATOR) {
            $str = "{$this->leftMember} {$this->operator}";
        } elseif ($this->operator == self::IS_NOT_NULL_OPERATOR) {
            $str = "{$this->leftMember} {$this->operator}";
        } elseif ($this->operator == self::IN_OPERATOR || $this->operator == self::NOT_IN_OPERATOR) {
            $str = "{$this->leftMember} {$this->operator} {$this->rightMember}";
        } elseif ($this->operator == self::FIND_IN_SET_OPERATOR) {
            $operatorReplaced = str_replace([
                '{SEARCH}',
                '{VALUES}',
            ], [
                $this->rightMember,
                $this->leftMember,
            ], $this->operator);
            $str = "{$operatorReplaced}";
        } else {
            $rightMemberAlias = $this->replacementValues['alias'];
            $rightWrapFunction = $this->getRightWrapFunction();
            if ($rightWrapFunction === null) {
                $str = "{$this->leftMember} {$this->operator} {$rightMemberAlias}";
            } else {
                $rightMemberElement = str_replace(self::REPLACEMENT_VALUE_ON_RIGHT_WRAP_FUNCTION, $rightMemberAlias, $rightWrapFunction);
                $str = "{$this->leftMember} {$this->operator} {$rightMemberElement}";
            }
        }

        $afterOperator = trim($this->afterOperator);

        if ($withAfterOperator && mb_strlen($afterOperator) > 0 && ($afterOperator === 'AND' || $afterOperator === 'OR')) {
            $str = "{$str} {$afterOperator}";
        }

        return $str;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->toString();
    }

    /**
     * @param string $a
     * @param int|float|string|bool|null $b
     * @param string $afterOperator
     * @param string $rightWrapFunction
     * @return WhereItem
     */
    public static function isEqual(string $a, $b, string $afterOperator = '', string $rightWrapFunction = null)
    {
        return new WhereItem($a, self::EQUAL_OPERATOR, $b, $afterOperator, $rightWrapFunction);
    }

    /**
     * @param string $a
     * @param int|float|string|bool|null $b
     * @param string $afterOperator
     * @param string $rightWrapFunction
     * @return WhereItem
     */
    public static function isNotEqual(string $a, $b, string $afterOperator = '', string $rightWrapFunction = null)
    {
        return new WhereItem($a, self::NOT_EQUAL_OPERATOR, $b, $afterOperator, $rightWrapFunction);
    }

    /**
     * @param string $a
     * @param int|float|string|bool|null $b
     * @param string $afterOperator
     * @param string $rightWrapFunction
     * @return WhereItem
     */
    public static function isGreaterThan(string $a, $b, string $afterOperator = '', string $rightWrapFunction = null)
    {
        return new WhereItem($a, self::GREATER_THAN_OPERATOR, $b, $afterOperator, $rightWrapFunction);
    }

    /**
     * @param string $a
     * @param int|float|string|bool|null $b
     * @param string $afterOperator
     * @param string $rightWrapFunction
     * @return WhereItem
     */
    public static function isGreaterThanOrEqualTo(string $a, $b, string $afterOperator = '', string $rightWrapFunction = null)
    {
        return new WhereItem($a, self::GREATER_OR_EQUAL_OPERATOR, $b, $afterOperator, $rightWrapFunction);
    }

    /**
     * @param string $a
     * @param int|float|string|bool|null $b
     * @param string $afterOperator
     * @param string $rightWrapFunction
     * @return WhereItem
     */
    public static function isLessThan(string $a, $b, string $afterOperator = '', string $rightWrapFunction = null)
    {
        return new WhereItem($a, self::LESS_THAN_OPERATOR, $b, $afterOperator, $rightWrapFunction);
    }

    /**
     * @param string $a
     * @param int|float|string|bool|null $b
     * @param string $afterOperator
     * @param string $rightWrapFunction
     * @return WhereItem
     */
    public static function isLessThanOrEqualTo(string $a, $b, string $afterOperator = '', string $rightWrapFunction = null)
    {
        return new WhereItem($a, self::LESS_OR_EQUAL_OPERATOR, $b, $afterOperator, $rightWrapFunction);
    }

    /**
     * @param string $a
     * @return WhereItem
     */
    public static function isNull(string $a)
    {
        return new WhereItem($a, self::IS_NULL_OPERATOR);
    }

    /**
     * @param string $a
     * @return WhereItem
     */
    public static function isNotNull(string $a)
    {
        return new WhereItem($a, self::IS_NOT_NULL_OPERATOR);
    }

    /**
     * @param string $a
     * @param string $b
     * @param string $afterOperator
     * @param string $rightWrapFunction
     * @return WhereItem
     */
    public static function like(string $a, string $b, string $afterOperator = '', string $rightWrapFunction = null)
    {
        return new WhereItem($a, self::LIKE_OPERATOR, $b, $afterOperator, $rightWrapFunction);
    }

    /**
     * @param string $a
     * @param string $b
     * @param string $afterOperator
     * @param string $rightWrapFunction
     * @return WhereItem
     */
    public static function notLike(string $a, string $b, string $afterOperator = '', string $rightWrapFunction = null)
    {
        return new WhereItem($a, self::NOT_LIKE_OPERATOR, $b, $afterOperator, $rightWrapFunction);
    }

    /**
     * @param string $a
     * @param int|float|string|bool|null $b
     * @param string $afterOperator
     * @param string $rightWrapFunction
     * @return WhereItem
     */
    public static function in(string $a, $b, string $afterOperator = '', string $rightWrapFunction = null)
    {
        return new WhereItem($a, self::IN_OPERATOR, $b, $afterOperator, $rightWrapFunction);
    }

    /**
     * @param string $haystack
     * @param string $needle
     * @param bool $addQuotes
     * @param bool $singleQuotes
     * @param string $afterOperator
     * @param string $rightWrapFunction
     * @return WhereItem
     */
    public static function findInSet(string $haystack, string $needle, bool $addQuotes = false, bool $singleQuotes = false, string $afterOperator = '', string $rightWrapFunction = null)
    {
        if ($addQuotes) {
            $quote = $singleQuotes ? "'" : '"';
            $haystack = "{$quote}{$haystack}{$quote}";
        }
        return new WhereItem($haystack, self::FIND_IN_SET_OPERATOR, $needle, $afterOperator, $rightWrapFunction);
    }

    /**
     * @param string $a
     * @param int|float|string|bool|null $b
     * @param string $afterOperator
     * @param string $rightWrapFunction
     * @return WhereItem
     */
    public static function notIn(string $a, $b, string $afterOperator = '', string $rightWrapFunction = null)
    {
        return new WhereItem($a, self::NOT_IN_OPERATOR, $b, $afterOperator, $rightWrapFunction);
    }

}
