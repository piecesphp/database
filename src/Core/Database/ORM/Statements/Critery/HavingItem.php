<?php
/**
 * HavingItem.php
 */
namespace PiecesPHP\Core\Database\ORM\Statements\Critery;

/**
 * HavingItem.
 *
 * @package     PiecesPHP\Core\Database\ORM\Statements\Critery
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2020
 */
class HavingItem extends WhereItem
{}
