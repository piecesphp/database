<?php
/**
 * WhereItemGroup.php
 */
namespace PiecesPHP\Core\Database\ORM\Statements\Critery;

/**
 * WhereItemGroup.
 *
 * @package     PiecesPHP\Core\Database\ORM\Statements\Critery
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2020
 */
class WhereItemGroup
{

    /**
     * @var WhereItem[]
     */
    protected $criteria = [];
    /**
     * @var bool
     */
    protected $withAfterOperator = true;

    /**
     * @param WhereItem[] $criteria
     */
    public function __construct(array $criteria)
    {
        $this->setCriteria($criteria);
    }

    /**
     * @param bool $with
     * @return bool|static
     */
    public function withAfterOperator(bool $with = null)
    {
        if ($with !== null) {
            $this->withAfterOperator = $with;
            return $this;
        } else {
            return $this->withAfterOperator;
        }
    }

    /**
     * @param WhereItem $critery
     * @return static
     */
    public function addCritery(WhereItem $critery)
    {
        $this->criteria[] = $critery;
        return $this;
    }

    /**
     * @param WhereItem[] $criteria
     * @return static
     */
    public function setCriteria(array $criteria)
    {
        $this->criteria = [];
        foreach ($criteria as $critery) {
            $this->addCritery($critery);
        }
        return $this;
    }

    /**
     * @return WhereItem[]
     */
    public function getCriteria()
    {
        return $this->criteria;
    }

    /**
     * @return int
     */
    public function countCriteria()
    {
        return count($this->criteria);
    }

    /**
     * @return array<string,int>|array<string,float>|array<string,string>|array<string,bool>|array<string,null>
     */
    public function getReplacementValues()
    {
        $replacementValues = [];

        foreach ($this->criteria as $critery) {
            $criteryReplacement = $critery->getReplacementValues();
            if (isset($criteryReplacement['alias']) && isset($criteryReplacement['value'])) {
                $replacementValues[$criteryReplacement['alias']] = $criteryReplacement['value'];
            }
        }

        return $replacementValues;
    }

    /**
     * @param bool $withAfterOperator
     * @return string
     */
    public function toString(bool $withAfterOperator = false)
    {
        $criteria = [];
        $countCriteria = $this->countCriteria();
        $lastIndex = array_key_last($this->criteria);
        $afterOperator = '';

        foreach ($this->criteria as $index => $critery) {

            $isLast = $index === $lastIndex;

            if ($countCriteria === 1 || $isLast) {
                $afterOperator = $critery->getAfterOperator();
                $criteria[] = $critery->toString(false);
            } else {
                $criteria[] = "{$critery}";
            }

        }

        $criteria = implode(" ", $criteria);

        $afterOperator = trim($afterOperator);

        if (($this->withAfterOperator() || $withAfterOperator) && (mb_strlen($afterOperator) > 0 && ($afterOperator === 'AND' || $afterOperator === 'OR'))) {
            $str = "({$criteria}) {$afterOperator}";
        } else {
            $str = "({$criteria})";
        }

        return $str;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->toString();
    }

}
