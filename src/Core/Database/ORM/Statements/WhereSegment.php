<?php
/**
 * WhereSegment.php
 */
namespace PiecesPHP\Core\Database\ORM\Statements;

use PiecesPHP\Core\Database\ORM\Statements\Critery\WhereItem;
use PiecesPHP\Core\Database\ORM\Statements\Critery\WhereItemGroup;

/**
 * WhereSegment.
 *
 * @package     PiecesPHP\Core\Database\ORM\Statements
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2020
 */
class WhereSegment
{
    /**
     * @var WhereItemGroup[]
     */
    protected $groups = [];

    /**
     * @param WhereItem[] $criteria
     */
    public function __construct(array $criteria = [])
    {
        $this->setCriteria($criteria);

    }

    /**
     * @param WhereItemGroup $group
     * @return static
     */
    public function addGroup(WhereItemGroup $group)
    {
        $this->groups[] = $group;
        return $this;
    }

    /**
     * @param WhereItem $critery
     * @return static
     */
    public function addCritery(WhereItem $critery)
    {
        $this->groups[] = new WhereItemGroup([$critery]);
        return $this;
    }

    /**
     * @return int
     */
    public function countCriteria()
    {
        $count = 0;
        foreach ($this->groups as $group) {
            $count += $group->countCriteria();
        }
        return $count;
    }

    /**
     * @param WhereItem[] $criteria
     * @return static
     */
    public function addCriteria(array $criteria)
    {
        $group = new WhereItemGroup($criteria);
        $this->groups[] = $group;
        return $this;
    }

    /**
     * @param WhereItem[] $criteria
     * @return static
     */
    public function setCriteria(array $criteria)
    {
        $this->groups = [];
        foreach ($criteria as $critery) {
            $this->addCritery($critery);
        }
        return $this;
    }

    /**
     * @return array<string,int>|array<string,float>|array<string,string>|array<string,bool>|array<string,null>
     */
    public function getReplacementValues()
    {
        $replacementValues = [];

        foreach ($this->groups as $group) {

            $criteriaReplacement = $group->getReplacementValues();

            foreach ($criteriaReplacement as $key => $value) {
                $replacementValues[$key] = $value;
            }

        }

        return $replacementValues;
    }

    /**
     * @return string
     */
    public function toString()
    {
        $groups = [];
        $countGroup = $this->countCriteria();
        $lasIndex = array_key_last($this->groups);

        foreach ($this->groups as $index => $group) {

            $isLast = $index === $lasIndex;

            if ($countGroup === 1 || $isLast) {
                $group->withAfterOperator(false);
            }

            $groups[] = "{$group}";

        }

        $groups = implode(" ", $groups);

        return "WHERE {$groups}";
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->toString();
    }

}
