<?php
/**
 * JoinSegment.php
 */
namespace PiecesPHP\Core\Database\ORM\Statements;

use PiecesPHP\Core\Database\ORM\Statements\Critery\JoinItem;

/**
 * JoinSegment.
 *
 * @package     PiecesPHP\Core\Database\ORM\Statements
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2020
 */
class JoinSegment
{

    const TYPE_INNER = 'INNER';
    const TYPE_LEFT = 'LEFT';
    const TYPE_RIGHT = 'RIGHT';
    const TYPE_NORMAL = 'NORMAL';

    /**
     * @var JoinItem[]
     */
    protected $criteria = [];
    /**
     * @var string
     */
    protected $table = '';
    /**
     * @var string
     */
    protected $type = '';

    /**
     * @param JoinItem[] $criteria
     * @param string $table
     */
    public function __construct(array $criteria = [], string $table = '', string $type = '')
    {
        $this->setCriteria($criteria);
        $this->setTable($table);
        $this->setType($type);
    }

    /**
     * @param string $type
     * @return static
     */
    public function setType(string $type)
    {
        $this->type = mb_strtoupper(trim($type));
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $table
     * @return static
     */
    public function setTable(string $table)
    {
        $this->table = $table;
        return $this;
    }

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @param JoinItem $critery
     * @return static
     */
    public function addCritery(JoinItem $critery)
    {
        $this->criteria[] = $critery;
        return $this;
    }

    /**
     * @return int
     */
    public function countCriteria()
    {
        return count($this->criteria);
    }

    /**
     * @param JoinItem[] $criteria
     * @return static
     */
    public function setCriteria(array $criteria)
    {
        $this->criteria = [];
        foreach ($criteria as $critery) {
            $this->addCritery($critery);
        }
        return $this;
    }

    /**
     * @return string
     */
    public function toString()
    {
        $criteria = [];
        $countCriteria = $this->countCriteria();
        $lasIndex = array_key_last($this->criteria);

        foreach ($this->criteria as $index => $critery) {

            $isLast = $index === $lasIndex;

            if ($countCriteria === 1 || $isLast) {
                $criteria[] = $critery->toString(false);
            } else {
                $criteria[] = "{$critery}";
            }

        }

        $criteria = implode(" ", $criteria);
        $type = $this->getType();
        $table = $this->getTable();

        $str = '';

        if ($type === self::TYPE_NORMAL) {
            $str = "JOIN {$table}";
        } else {
            $str = "{$type} JOIN {$table}";
        }

        if ($countCriteria > 0) {
            $str .= " ON {$criteria}";
        }

        return $str;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->toString();
    }

}
