<?php
/**
 * InsertSegment.php
 */
namespace PiecesPHP\Core\Database\ORM\Statements;

use Exception;

/**
 * InsertSegment.
 *
 * @package     PiecesPHP\Core\Database\ORM\Statements
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2020
 */
class InsertSegment
{
    /**
     * @var string
     */
    protected $table = '';
    /**
     * @var array<string,string>
     */
    protected $data = [];
    /**
     * @var array<string,int>|array<string,float>|array<string,string>|array<string,bool>|array<string,null>
     */
    protected $replacementValues = [];
    /**
     * @var callable
     * @return string
     */
    protected $scapeReservedWordsStrategy = null;

    /**
     * @param array<string,int>|array<string,float>|array<string,string>|array<string,bool>|array<string,null> $map
     * @param string $table
     */
    public function __construct(array $map, string $table = '')
    {

        $this->table = $table;

        $this->scapeReservedWordsStrategy = function ($word) {

            $word = trim($word);

            if ($word !== '*') {
                $word = "`{$word}`";
            }

            return $word;
        };

        $this->setData($map);

    }

    /**
     * Función usada para escapar los valores individuales que representan los campos
     *
     * @param callable $function Debe recibir un string y devolver un string que contenga el recibido
     * @return static
     */
    public function setScapeReservedWordStrategy(callable $function)
    {

        $testString = 'testing';
        $testOutput = ($function)($testString);

        if (is_string($testOutput) && strpos($testOutput, $testString) !== false) {
            $this->scapeReservedWordsStrategy = $function;
        }

        return $this;

    }

    /**
     * @param string $field
     * @param int|float|string|bool|null $value
     * @param bool $scape
     * @return static
     */
    public function addData(string $field, $value, bool $scape = false)
    {
        $field = $scape ? ($this->scapeReservedWordsStrategy)($field) : $field;

        if (mb_strlen($field) > 0) {

            $base = str_replace([' ', '`', '"', "'"], '', $field);
            $base = preg_replace('|\&.*\;|', '', htmlentities($base));
            $replacementParam = preg_replace("/[.|\(|\)]/", '', ':INSERT' . uniqid() . "_{$base}");
            $replacementParam = mb_strtoupper($replacementParam);

            if (is_scalar($value) || $value === null) {

                $this->data[$field] = $replacementParam;
                $this->replacementValues[$replacementParam] = $value;

            } else {
                throw new Exception('$value es obligatorio');
            }

        } else {
            throw new Exception('$field es obligatorio');
        }

        return $this;
    }

    /**
     * @param array<string,int>|array<string,float>|array<string,string>|array<string,bool>|array<string,null> $map
     * @param bool $scape
     * @return static
     */
    public function setData(array $map, bool $scape = false)
    {
        $this->data = [];
        $this->replacementValues = [];
        foreach ($map as $field => $value) {
            $this->addData($field, $value, $scape);
        }
        return $this;
    }

    /**
     * @param string $table
     * @return static
     */
    public function setTable(string $table)
    {
        $this->table = $table;
        return $this;
    }

    /**
     * @return array<string,int>|array<string,float>|array<string,string>|array<string,bool>|array<string,null>
     */
    public function getReplacementValues()
    {
        return $this->replacementValues;
    }

    /**
     * @return string
     */
    public function toString()
    {
        $fields = [];
        $aliases = [];

        foreach ($this->data as $field => $alias) {
            $fields[] = $field;
            $aliases[] = $alias;
        }

        $fields = implode(', ', $fields);
        $aliases = implode(', ', $aliases);

        return "INSERT INTO {$this->table} ({$fields}) VALUES ({$aliases})";
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->toString();
    }

}
