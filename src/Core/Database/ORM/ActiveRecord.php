<?php

/**
 * ActiveRecord.php
 */
namespace PiecesPHP\Core\Database\ORM;

use Exception;
use PDOStatement;
use PiecesPHP\Core\Database\Database;
use PiecesPHP\Core\Database\ORM\Statements\Critery\HavingItem;
use PiecesPHP\Core\Database\ORM\Statements\Critery\JoinItem;
use PiecesPHP\Core\Database\ORM\Statements\Critery\WhereItem;
use PiecesPHP\Core\Database\ORM\Statements\Critery\WhereItemGroup;
use PiecesPHP\Core\Database\ORM\Statements\HavingSegment;
use PiecesPHP\Core\Database\ORM\Statements\InsertSegment;
use PiecesPHP\Core\Database\ORM\Statements\JoinSegment;
use PiecesPHP\Core\Database\ORM\Statements\SelectQuerySegment;
use PiecesPHP\Core\Database\ORM\Statements\UpdateSegment;
use PiecesPHP\Core\Database\ORM\Statements\WhereSegment;

/**
 * ActiveRecord
 *
 * @package     PiecesPHP\Core\Database\ORM
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2018
 */
class ActiveRecord
{

    const ACTION_INSERT = 'INSERT';
    const ACTION_SELECT = 'SELECT';
    const ACTION_UPDATE = 'UPDATE';
    const ACTION_DELETE = 'DELETE';

    /**
     * @var SelectQuerySegment|string|null
     */
    protected $selectQuery = null;
    /**
     * @var InsertSegment|array|null
     */
    protected $insertSegment = null;
    /**
     * @var UpdateSegment|array|null
     */
    protected $updateSegment = null;
    /**
     * @var WhereSegment|string|null
     */
    protected $whereSegment = null;
    /**
     * @var HavingSegment|string|null
     */
    protected $havingSegment = null;
    /**
     * @var JoinSegment|string|null
     */
    protected $joinSegment = null;
    /**
     * @var string|null
     */
    protected $groupByString = null;
    /**
     * @var string|null
     */
    protected $orderByString = null;
    /**
     * @var bool Permite ejecutar UPDATE sin WHERE
     */
    protected $insecureMode = false;
    /**
     * @var string
     */
    protected $lastSQLExecuted = '';
    /**
     * @var string
     */
    protected $currentAction = '';
    /**
     * @var string
     */
    protected $sql = '';
    /**
     * @var array|null
     */
    protected $resultSet = null;
    /**
     * @var bool
     */
    protected $typeResultAssoc = false;
    /**
     * @var Database|null
     */
    protected $database = null;
    /**
     * @var PDOStatement|null
     */
    protected $prepareStatement = null;
    /**
     * @var string
     */
    protected $table = '';
    /**
     * @var string[]
     */
    protected $fields = [];
    /**
     * @var string|null Nombre de la clase de referencia para fetchAll
     */
    protected $selectClass = null;
    /**
     *  @var array
     */
    protected $replacePrepareValues = [];
    /**
     *  @var array
     */
    protected $whereReplacePrepareValues = [];
    /**
     *  @var array
     */
    protected $havingReplacePrepareValues = [];

    /**
     * @param Database $database
     * @param string $table
     */
    public function __construct(Database $database = null, string $table = '')
    {
        if ($database !== null) {
            $this->database = $database;
        }

        if (mb_strlen(trim($table)) > 0) {
            $this->table = $table;
        }
    }

    /**
     * @param Database $database
     * @return static
     */
    public function setDatabase(Database $database)
    {
        $this->database = $database;
        return $this;
    }

    /**
     * @return Database
     */
    public function getDatabase()
    {
        return $this->database;
    }

    /**
     * @param string $sql
     * @return \PDOStatement
     */
    public function prepare(string $sql)
    {
        return $this->database->prepare($sql);
    }

    /**
     * @param SelectQuerySegment|string[]|string $fields
     * @return static
     */
    public function select($fields = null)
    {
        if (is_string($fields)) {
            $this->selectQuery = trim($fields);
        } elseif ($fields instanceof SelectQuerySegment) {
            $this->selectQuery = $fields;
        } elseif (is_array($fields)) {
            $this->selectQuery = new SelectQuerySegment($fields);
        } elseif ($fields === null) {
            $this->selectQuery = new SelectQuerySegment(mb_strlen($this->table) > 0 ? $this->getFields() : ['*']);
        } else {
            throw new Exception('Parámetro no aceptado en select()');
        }
        $this->currentAction = self::ACTION_SELECT;
        return $this;
    }

    /**
     * @param InsertSegment|array<string,int>|array<string,float>|array<string,string>|array<string,bool>|array<string,null> $data
     * @return static
     */
    public function insert($data)
    {
        if (is_array($data)) {
            $this->insertSegment = new InsertSegment($data);
        } elseif ($data instanceof InsertSegment) {
            $this->insertSegment = $data;
        } else {
            throw new Exception('Parámetro no aceptado en insert()');
        }
        $this->replacePrepareValues = $this->insertSegment->getReplacementValues();
        $this->currentAction = self::ACTION_INSERT;
        return $this;
    }

    /**
     * @param UpdateSegment|array<string,int>|array<string,float>|array<string,string>|array<string,bool>|array<string,null> $data
     * @param bool $insecureMode Permite ejecutar el UPDATE sin WHERE
     * @return static
     */
    public function update($data, bool $insecureMode = false)
    {
        if (is_array($data)) {
            $this->updateSegment = new UpdateSegment($data);
        } elseif ($data instanceof UpdateSegment) {
            $this->updateSegment = $data;
        } else {
            throw new Exception('Parámetro no aceptado en update()');
        }
        $this->replacePrepareValues = $this->updateSegment->getReplacementValues();
        $this->insecureMode = $insecureMode;
        $this->currentAction = self::ACTION_UPDATE;
        return $this;
    }

    /**
     * @param WhereSegment|string|array $where
     * @return static
     */
    public function delete($where)
    {
        $this->currentAction = self::ACTION_DELETE;
        return $this->where($where);
    }

    /**
     * @param string $table
     * @param JoinSegment|string|array $on
     * @param string $type LEFT|INNER|RIGHT|NORMAL
     * @return static
     */
    public function join(string $table, $on = [], string $type = JoinSegment::TYPE_NORMAL)
    {

        if ($on instanceof JoinSegment) {

            $on->setTable($table);
            $on->setType($type);

            $this->joinSegment = $on;

        } elseif (is_string($on)) {

            $this->joinSegment = $type === JoinSegment::TYPE_NORMAL ? "JOIN {$table} ON ({$on})" : "{$type} JOIN {$table} ON ({$on})";

        } elseif (is_array($on)) {

            $joinSegment = new JoinSegment([], $table, $type);
            $joinSegment->setTable($table);
            $joinSegment->setType($type);

            foreach ($on as $column => $value) {

                //Verifica que $column sea string puesto que debe corresponder al nombre de columna
                if (is_string($column)) {

                    //Valores que representan cada criterio
                    $values = [];

                    //Verifica si el valor provisto corresponde a diferentes comparaciones de una misma columna
                    if (is_array($value) && array_key_exists('multiple', $value) && is_array($value['multiple'])) {
                        foreach ($value['multiple'] as $multiple_value) {
                            $values[] = $multiple_value;
                        }
                    } else {
                        //Si es un valor único, lo agrega a la lista para garantizar compatibilidad con las validaciones que esperan un array
                        $values[] = $value;
                    }

                    foreach ($values as $options) {

                        $defaulOptions = [
                            JoinItem::EQUAL_OPERATOR => $value,
                            'and_or' => JoinItem::AND_OPERATOR,
                        ];

                        //Las opciones del criterio
                        $options = is_array($options) ? $options : $defaulOptions;

                        //Verifica que $options contenga 2 elementos
                        if (count($options) === 2) {

                            $andOrOr = isset($options['and_or']) && is_string($options['and_or']) ? mb_strtoupper($options['and_or']) : JoinItem::AND_OPERATOR;
                            $andOrOr = in_array($andOrOr, [JoinItem::AND_OPERATOR, JoinItem::OR_OPERATOR]) ? $andOrOr : JoinItem::AND_OPERATOR;

                            unset($options['and_or']);

                            foreach ($options as $key => $needValue) {

                                $joinItem = new JoinItem($column, $key, $needValue, $andOrOr);
                                $joinSegment->addCritery($joinItem);

                            }

                        } else {
                            throw new Exception('Parámetro no aceptado en join(). El valor del criterio entrante está mal configurado.');
                        }
                    }

                } else {
                    throw new Exception('Parámetro no aceptado en join(). Las llaves de los elementos del array deben ser el nombre de la columna.');
                }

            }

            $this->joinSegment = $joinSegment;

        } else {
            throw new Exception('Parámetro no aceptado en join()');
        }

        return $this;
    }

    /**
     * @param string $table
     * @param JoinSegment|string|array $on
     * @return static
     */
    public function leftJoin(string $table, $on = [])
    {
        return $this->join($table, $on, JoinSegment::TYPE_LEFT);
    }

    /**
     * @param string $table
     * @param JoinSegment|string|array $on
     * @return static
     */
    public function rightJoin(string $table, $on = [])
    {
        return $this->join($table, $on, JoinSegment::TYPE_RIGHT);
    }

    /**
     * @param string $table
     * @param JoinSegment|string|array $on
     * @return static
     */
    public function innerJoin(string $table, $on = [])
    {
        return $this->join($table, $on, JoinSegment::TYPE_INNER);
    }

    /**
     * @param WhereSegment|string|array $where
     * @return static
     */
    public function where($where)
    {

        if ($where instanceof WhereSegment) {

            $this->whereSegment = $where;
            $this->whereReplacePrepareValues = $this->whereSegment->getReplacementValues();

        } elseif (is_string($where)) {

            $this->whereSegment = "WHERE ({$where})";

        } elseif (is_array($where)) {

            $whereSegment = new WhereSegment();

            foreach ($where as $column => $value) {

                //Verifica que $column sea string puesto que debe corresponder al nombre de columna
                if (is_scalar($column)) {

                    //Valores que representan cada criterio
                    $values = [];
                    $isGroup = false;

                    //Verifica si el valor provisto corresponde a diferentes comparaciones de una misma columna
                    if (is_array($value) && array_key_exists('multiple', $value) && is_array($value['multiple'])) {
                        foreach ($value['multiple'] as $multiple_value) {
                            $values[] = $multiple_value;
                        }
                        $isGroup = true;
                    } else {
                        //Si es un valor único, lo agrega a la lista para garantizar compatibilidad con las validaciones que esperan un array
                        $values[] = $value;
                    }

                    $group = new WhereItemGroup([]);

                    foreach ($values as $options) {

                        $defaulOptions = [
                            WhereItem::EQUAL_OPERATOR => $value,
                            'and_or' => WhereItem::AND_OPERATOR,
                        ];

                        //Las opciones del criterio
                        $options = is_array($options) ? $options : $defaulOptions;
                        if (!array_key_exists('and_or', $options)) {
                            $options['and_or'] = $defaulOptions['and_or'];
                        }

                        //Verifica que $options contenga 2 elementos
                        if (count($options) === 2) {

                            $andOrOr = isset($options['and_or']) && is_string($options['and_or']) ? mb_strtoupper($options['and_or']) : WhereItem::AND_OPERATOR;
                            $andOrOr = in_array($andOrOr, [WhereItem::AND_OPERATOR, WhereItem::OR_OPERATOR]) ? $andOrOr : WhereItem::AND_OPERATOR;

                            unset($options['and_or']);

                            foreach ($options as $key => $needValue) {

                                $whereItem = new WhereItem($column, $key, $needValue, $andOrOr);

                                if ($isGroup) {
                                    $group->addCritery($whereItem);
                                } else {
                                    $whereSegment->addCritery($whereItem);
                                }

                            }

                        } else {
                            throw new Exception('Parámetro no aceptado en where(). El valor del criterio entrante está mal configurado.');
                        }
                    }

                    if ($isGroup) {
                        $whereSegment->addCriteria($group->getCriteria());
                    }

                } else {
                    throw new Exception('Parámetro no aceptado en where(). Las llaves de los elementos del array deben ser el nombre de la columna.');
                }

            }

            $this->whereSegment = $whereSegment;
            $this->whereReplacePrepareValues = $this->whereSegment->getReplacementValues();

        } else {
            throw new Exception('Parámetro no aceptado en where()');
        }

        return $this;
    }

    /**
     * @param HavingSegment|string|array $having
     * @return static
     */
    public function having($having)
    {

        if ($having instanceof HavingSegment) {

            $this->havingSegment = $having;
            $this->havingReplacePrepareValues = $this->havingSegment->getReplacementValues();

        } elseif (is_string($having)) {

            $this->havingSegment = "HAVING ({$having})";

        } elseif (is_array($having)) {

            $havingSegment = new HavingSegment();

            foreach ($having as $column => $value) {

                //Verifica que $column sea string puesto que debe corresponder al nombre de columna
                if (is_string($column)) {

                    //Valores que representan cada criterio
                    $values = [];

                    //Verifica si el valor provisto corresponde a diferentes comparaciones de una misma columna
                    if (is_array($value) && array_key_exists('multiple', $value) && is_array($value['multiple'])) {
                        foreach ($value['multiple'] as $multiple_value) {
                            $values[] = $multiple_value;
                        }
                    } else {
                        //Si es un valor único, lo agrega a la lista para garantizar compatibilidad con las validaciones que esperan un array
                        $values[] = $value;
                    }

                    foreach ($values as $options) {

                        $defaulOptions = [
                            HavingItem::EQUAL_OPERATOR => $value,
                            'and_or' => HavingItem::AND_OPERATOR,
                        ];

                        //Las opciones del criterio
                        $options = is_array($options) ? $options : $defaulOptions;

                        //Verifica que $options contenga 2 elementos
                        if (count($options) === 2) {

                            $andOrOr = isset($options['and_or']) && is_string($options['and_or']) ? mb_strtoupper($options['and_or']) : HavingItem::AND_OPERATOR;
                            $andOrOr = in_array($andOrOr, [HavingItem::AND_OPERATOR, HavingItem::OR_OPERATOR]) ? $andOrOr : HavingItem::AND_OPERATOR;

                            unset($options['and_or']);

                            foreach ($options as $key => $needValue) {

                                $havingItem = new HavingItem($column, $key, $needValue, $andOrOr);
                                $havingSegment->addCritery($havingItem);

                            }

                        } else {
                            throw new Exception('Parámetro no aceptado en having(). El valor del criterio entrante está mal configurado.');
                        }
                    }

                } else {
                    throw new Exception('Parámetro no aceptado en having(). Las llaves de los elementos del array deben ser el nombre de la columna.');
                }

            }

            $this->havingSegment = $havingSegment;
            $this->havingReplacePrepareValues = $this->havingSegment->getReplacementValues();

        } else {
            throw new Exception('Parámetro no aceptado en having()');
        }

        return $this;
    }

    /**
     * Ejecuta la consulta de tipo SELECT que esté preparada.
     * @param bool $assoc Define si el resultado es un array asociativo o un objeto
     * @return boolean|object|array|int
     * Devuelve un objeto correspondiente al primer registro que devuelva la consulta.
     * Si la consulta preparada no es de tipo SELECT devuelve false.
     * Si la consulta no da resultados devuelve -1.
     * Si la consulta falla devuelve false.
     */
    public function row(bool $assoc = false)
    {

        if ($this->currentAction === self::ACTION_SELECT) {

            $status = $this->_executeSelect($assoc);

            if ($status) {

                $fetch = $this->resultSet;

                if (is_array($fetch) && count($fetch) > 0) {
                    $fetch = $fetch[0];
                } else {
                    $fetch = -1;
                }

                return $fetch;

            } else {
                return false;
            }

        } else {
            return false;
        }

    }

    /**
     * @param bool $assoc
     * @return false|array
     */
    public function getAll(bool $assoc = false)
    {
        $this->prepareStatement = $this->prepare("SELECT * FROM {$this->table}");

        $status = $this->prepareStatement->execute();

        if ($status) {

            if ($this->typeResultAssoc === true || $assoc === true) {
                $fetch = $this->prepareStatement->fetchAll(Database::FETCH_ASSOC);
            } else {
                $fetch = $this->prepareStatement->fetchAll(Database::FETCH_OBJ);
            }

            $this->prepareStatement->closeCursor();

            return $fetch;

        } else {
            return false;
        }

    }

    /**
     * @param mixed $value
     * @param string $columnName
     * @param string|array|SelectQuerySegment $columnsOnSelect
     * @param bool $assoc
     *
     * @return bool|object|array|int
     * Devuelve un objeto correspondiente al primer registro que devuelva la consulta.
     * Si la consulta no da resultados devuelve -1.
     * Si la consulta falla devuelve false.
     */
    public function get($value, string $columnName = 'id', $columnsOnSelect = '*', bool $assoc = false)
    {
        $fetch = null;

        $selectString = "";

        if (is_string($columnsOnSelect)) {

            $selectString = "SELECT {$columnsOnSelect} FROM {$this->table}";

        } elseif ($columnsOnSelect instanceof SelectQuerySegment) {

            $columnsOnSelect = $columnsOnSelect;
            $columnsOnSelect->setTable($this->table);
            $selectString = "{$columnsOnSelect}";

        } elseif (is_array($columnsOnSelect)) {

            $columnsOnSelect = new SelectQuerySegment($columnsOnSelect, $this->table);
            $selectString = "{$columnsOnSelect}";

        } else {
            throw new Exception('Parámetro $columnsOnSelect erróneo');
        }

        $this->prepareStatement = $this->prepare("{$selectString} WHERE {$columnName} = :VALUE_WHERE");

        $status = $this->prepareStatement->execute([
            ':VALUE_WHERE' => $value,
        ]);

        if ($status) {

            if ($this->typeResultAssoc === true || $assoc === true) {
                $fetch = $this->prepareStatement->fetchAll(Database::FETCH_ASSOC);
            } else {
                $fetch = $this->prepareStatement->fetchAll(Database::FETCH_OBJ);
            }

            if (count($fetch) > 0) {
                $fetch = $fetch[0];
            } else {
                $fetch = -1;
            }

        } else {
            $fetch = false;
        }

        $this->prepareStatement->closeCursor();

        return $fetch;

    }

    /**
     * @param string|string[] $groupString un string o array de string que representa
     * los criterios de GROUP BY (sin incluir GROUP BY)
     * @return static
     */
    public function groupBy($groupString)
    {
        $groupString = is_array($groupString) ? $groupString : [$groupString];
        $groupString = array_map(function ($e) {
            return trim($e);
        }, array_filter($groupString, function ($e) {
            return is_string($e);
        }));
        $groupString = trim(implode(', ', $groupString));
        $this->groupByString = "GROUP BY $groupString";
        return $this;
    }

    /**
     * @param string|string[] $orderString un string o array de string que representa
     * los criterios de ORDER BY (sin incluir ORDER BY)
     * @return static
     */
    public function orderBy($orderString)
    {
        $orderString = is_array($orderString) ? $orderString : [$orderString];
        $orderString = array_map(function ($e) {
            return trim($e);
        }, array_filter($orderString, function ($e) {
            return is_string($e);
        }));
        $orderString = trim(implode(', ', $orderString));
        $this->orderByString = "ORDER BY $orderString";
        return $this;
    }

    /**
     * Último registro insertado.
     * @param string $name
     * @return mixed
     */
    public function lastInsertId(string $name = null)
    {
        return $this->database->lastInsertId($name);
    }

    /**
     * Cantidad de registros de una tabla según una columna.
     * Funciona igual que 'SELECT COUNT(column) FROM table' por lo que se debe usa sobre una columna
     * de la cual se esté seguro que posea un valor en todos los registros
     * para obtener la cantidad real de registros.
     * @param string $column Columna que se usará de referencia para el conteo
     * @return int
     */
    public function rowCount(string $column = 'id')
    {
        $this->prepareStatement = $this->prepare("SELECT COUNT({$column}) AS total FROM {$this->table}");
        $this->prepareStatement->execute();
        $result = $this->prepareStatement->fetchAll(Database::FETCH_OBJ);
        $result = count($result) > 0 ? (int) $result[0]->total : 0;
        $this->prepareStatement->closeCursor();
        return $result;
    }

    /**
     * @param string $table
     * @return static
     */
    public function setTable(string $table)
    {
        $this->table = trim($table);
        return $this;
    }

    /**
     * @param string[] $fields
     * @return static
     */
    public function setFields(array $fields)
    {
        $this->fields = array_map(function ($field) {
            return trim($field);
        }, array_filter($fields, function ($field) {
            return is_string($field);
        }));
        return $this;
    }

    /**
     * setTypeResult
     * @param bool $assoc Si es true los resultados de las consultas serán un array asociativo
     * @return static
     */
    public function setTypeResult(bool $assoc = true)
    {
        $this->typeResultAssoc = $assoc;
        return $this;
    }

    /**
     * @param string $nameClass
     * @return static
     */
    public function setSelectClass(string $nameClass)
    {
        $this->selectClass = $nameClass;
        return $this;
    }

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @return string[]
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Devuelve el string de la consulta
     * @param bool $withValuesReplaced
     * @return string
     */
    public function getCompiledSQL(bool $withValuesReplaced = false)
    {
        $query = '';

        switch ($this->currentAction) {
            case self::ACTION_INSERT:
                $query = $this->structureQuery('INSERT', false);
                break;
            case self::ACTION_SELECT:
                $query = $this->structureQuery('SELECT', false);
                break;
            case self::ACTION_UPDATE:
                $query = $this->structureQuery('UPDATE', false);
                break;
            case self::ACTION_DELETE:
                $query = $this->structureQuery('DELETE', false);
                break;
        }

        $replaceValues = $this->replacePrepareValues;
        $replaceValuesWhere = $this->whereReplacePrepareValues;
        $replaceValuesHaving = $this->havingReplacePrepareValues;
        $replace = function (string $alias, $value, string $query, bool $interpolate = false) {

            $baseAlias = str_replace(':', "", $alias);

            if (!$interpolate) {
                $query = preg_replace("/" . $alias . "/", "($baseAlias=$value)", $query);
            } else {
                if (is_string($value)) {
                    $value = $this->database->quote($value);
                }
                $query = preg_replace("/" . $alias . "/", $value, $query);
            }

            return $query;

        };

        foreach ($replaceValuesWhere as $alias => $value) {
            $query = ($replace)($alias, $value, $query, $withValuesReplaced);
        }

        foreach ($replaceValuesHaving as $alias => $value) {
            $query = ($replace)($alias, $value, $query, $withValuesReplaced);
        }

        foreach ($replaceValues as $alias => $value) {
            $query = ($replace)($alias, $value, $query, $withValuesReplaced);
        }

        $query = str_replace('  ', " ", $query);

        return $query;
    }

    /**
     * Vuelca un comando preparado de SQL
     * @return static
     */
    public function debugDumpParams()
    {
        if ($this->prepareStatement !== null) {
            $this->prepareStatement->debugDumpParams();
        }
        return $this;
    }

    /**
     * @return array
     */
    public function getReplaceValues()
    {
        $replaceValues = [];

        foreach ($this->replacePrepareValues as $key => $value) {
            $replaceValues[$key] = $value;
        }

        foreach ($this->whereReplacePrepareValues as $key => $value) {
            $replaceValues[$key] = $value;
        }

        foreach ($this->havingReplacePrepareValues as $key => $value) {
            $replaceValues[$key] = $value;
        }

        return $replaceValues;
    }

    /**
     * @return array
     */
    public function getReplaceWhereAndHavingValues()
    {
        $replaceValues = [];

        foreach ($this->whereReplacePrepareValues as $key => $value) {
            $replaceValues[$key] = $value;
        }

        foreach ($this->havingReplacePrepareValues as $key => $value) {
            $replaceValues[$key] = $value;
        }

        return $replaceValues;
    }

    /**
     * @return static
     */
    public function resetJoins()
    {
        $this->joinSegment = null;
        return $this;
    }

    /**
     * @return static
     */
    public function resetWhere()
    {
        $this->whereSegment = null;
        $this->whereReplacePrepareValues = [];
        return $this;
    }

    /**
     * @return static
     */
    public function resetHaving()
    {
        $this->havingSegment = null;
        $this->havingReplacePrepareValues = [];
        return $this;
    }

    /**
     * @return static
     */
    public function resetAll()
    {
        $this->selectQuery = null;
        $this->insertSegment = null;
        $this->updateSegment = null;
        $this->whereSegment = null;
        $this->havingSegment = null;
        $this->joinSegment = null;
        $this->groupByString = null;
        $this->orderByString = null;
        $this->insecureMode = false;
        $this->currentAction = '';
        $this->sql = '';
        $this->typeResultAssoc = false;
        $this->prepareStatement = null;
        $this->selectClass = null;
        $this->replacePrepareValues = [];
        $this->whereReplacePrepareValues = [];
        $this->havingReplacePrepareValues = [];
        return $this;
    }

    /**
     * @return string
     */
    public function getLastSQLExecuted()
    {
        return $this->lastSQLExecuted;
    }

    /**
     * Ejecuta la consulta que esté preparada.
     * @param bool $assoc En caso de SELECT define si el resultado es un array asociativo o un objeto
     * @param int $page En caso de SELECT define la "página de inicio" de la consulta (debe definirse $perPage)
     * @param int $perPage En caso de SELECT define la "cantidad de registros por página" de la consulta (debe definirse $page)
     * @return bool|null
     * Devuelve null si no se ha ha llamado ninguno de los métodos select|insert|update|delete.
     * Devuelve true en caso de exito o false en caso de error.
     */
    public function execute(bool $assoc = false, int $page = null, int $perPage = null)
    {

        $result = null;

        switch ($this->currentAction) {

            case self::ACTION_INSERT:

                $result = $this->_executeInsert();

                break;

            case self::ACTION_SELECT:

                $assoc = $this->typeResultAssoc === true || $assoc === true;
                $result = $this->_executeSelect($assoc, $page, $perPage);

                break;

            case self::ACTION_UPDATE:

                $result = $this->_executeUpdate();

                break;

            case self::ACTION_DELETE:

                $result = $this->_executeDelete();

                break;

        }

        $this->prepareStatement = null;
        $this->resetAll();

        return $result;
    }

    /**
     * @return array|null
     */
    public function result()
    {
        return $this->resultSet;
    }

    /**
     * Ejecuta la selección.
     * @param bool $assoc Define si el resultado es un array asociativo o un objeto
     * @param int $page Define la "página de inicio" de la consulta (debe definirse $perPage)
     * @param int $perPage Define la "cantidad de registros por página" de la consulta (debe definirse $page)
     * @access private
     * @return bool
     * @throws \PDOException
     */
    private function _executeSelect(bool $assoc = false, int $page = null, int $perPage = null)
    {
        $this->structureQuery('SELECT', true, $page, $perPage);

        if ($this->prepareStatement === null) {
            return false;
        }

        $replaceValues = $this->getReplaceWhereAndHavingValues();
        $this->lastSQLExecuted = $this->sql;

        if (count($replaceValues) > 0) {
            $status = $this->prepareStatement->execute($replaceValues);
        } else {
            $status = $this->prepareStatement->execute();
        }

        if ($status) {

            if ($this->typeResultAssoc === true || $assoc === true) {
                $fetch = $this->prepareStatement->fetchAll(Database::FETCH_ASSOC);
            } else {
                if ($this->selectClass !== null) {
                    $fetch = $this->prepareStatement->fetchAll(Database::FETCH_CLASS, $this->selectClass);
                } else {
                    $fetch = $this->prepareStatement->fetchAll(Database::FETCH_OBJ);
                }
            }

            $this->resultSet = $fetch;

            $this->prepareStatement->closeCursor();
        } else {
            $this->resultSet = null;
        }

        return $status;
    }

    /**
     * @access private
     * @return bool
     * @throws \PDOException
     */
    private function _executeInsert()
    {
        $this->structureQuery('INSERT');
        $this->lastSQLExecuted = $this->sql;
        if ($this->prepareStatement === null) {
            return false;
        }
        $execute = $this->prepareStatement->execute($this->replacePrepareValues);
        $this->prepareStatement->closeCursor();
        return $execute;
    }

    /**
     * Ejecuta la actualización.
     * @access private
     * @return bool
     * @throws \PDOException
     */
    private function _executeUpdate()
    {
        $this->structureQuery('UPDATE');
        $this->lastSQLExecuted = $this->sql;
        $replacement = $this->getReplaceValues();
        if ($this->prepareStatement === null) {
            return false;
        }
        $execute = $this->prepareStatement->execute($replacement);
        $this->prepareStatement->closeCursor();
        return $execute;
    }

    /**
     * Ejecuta la eliminación.
     * @access private
     * @return bool
     * @throws \PDOException
     */
    private function _executeDelete()
    {
        $this->structureQuery('DELETE');
        $this->lastSQLExecuted = $this->sql;
        if ($this->prepareStatement === null) {
            return false;
        }
        if (count($this->getReplaceWhereAndHavingValues()) > 0) {
            $execute = $this->prepareStatement->execute($this->getReplaceWhereAndHavingValues());
        } else {
            $execute = $this->prepareStatement->execute();
        }
        $this->prepareStatement->closeCursor();
        return $execute;
    }

    /**
     * @param string $type
     * @param bool $prepare
     * @param int $page En caso de SELECT define la "página de inicio" de la consulta (debe definirse $perPage)
     * @param int $perPage En caso de SELECT define la "cantidad de registros por página" de la consulta (debe definirse $page)
     * @return string
     */
    private function structureQuery(string $type, bool $prepare = true, int $page = null, int $perPage = null)
    {
        $type = mb_strtoupper($type);

        $joinString = '';
        $whereString = '';
        $groupByString = '';
        $havingString = '';
        $orderByString = '';

        if (is_string($this->joinSegment) && mb_strlen($this->joinSegment) > 0) {
            $joinString = " {$this->joinSegment}";
        } elseif ($this->joinSegment instanceof JoinSegment) {
            $joinString = " {$this->joinSegment}";
        }

        if (is_string($this->whereSegment) && mb_strlen($this->whereSegment) > 0) {
            $whereString = " {$this->whereSegment}";
        } elseif ($this->whereSegment instanceof WhereSegment) {
            $whereString = " {$this->whereSegment}";
        }

        if (is_string($this->groupByString) && mb_strlen($this->groupByString) > 0) {
            $groupByString = " {$this->groupByString}";
        }

        if (is_string($this->havingSegment) && mb_strlen($this->havingSegment) > 0) {
            $havingString = " {$this->havingSegment}";
        } elseif ($this->havingSegment instanceof HavingSegment) {
            $havingString = " {$this->havingSegment}";
        }

        if (is_string($this->orderByString) && mb_strlen($this->orderByString) > 0) {
            $orderByString = " {$this->orderByString}";
        }

        switch ($type) {

            case self::ACTION_SELECT:

                $selectQuery = '';

                if ($this->selectQuery instanceof SelectQuerySegment) {
                    $this->selectQuery->setTable($this->table);
                    $selectQuery .= $this->selectQuery;
                } else {
                    $selectQuery = "SELECT {$this->selectQuery} FROM {$this->table} ";
                }

                $this->sql = "{$selectQuery}{$joinString}{$whereString}{$groupByString}{$havingString}{$orderByString}";

                if (!is_null($page) && !is_null($perPage)) {
                    $page = $page > 0 ? $page : 1;
                    $perPage = $perPage > 0 ? $perPage : 1;
                    $from = ($page - 1) * $perPage;
                    $this->sql .= " LIMIT $from, $perPage";
                }

                break;

            case self::ACTION_INSERT:

                if ($this->insertSegment instanceof InsertSegment) {
                    $this->insertSegment->setTable($this->table);
                }

                $this->sql = "{$this->insertSegment}{$joinString}{$whereString}{$havingString}";

                break;

            case self::ACTION_UPDATE:

                if ($this->updateSegment instanceof UpdateSegment) {
                    $this->updateSegment->setTable($this->table);
                }

                if (mb_strlen($whereString) == 0 && mb_strlen($havingString) == 0 && !$this->insecureMode) {
                    throw new Exception("Su consulta UPDATE no tiene WHERE ni HAVING, eso es peligroso. Si aún así quiere seguir, active la opción \$insecureMode en true.");
                }

                $this->sql = "{$this->updateSegment}{$joinString}{$whereString}{$havingString}";

                break;

            case self::ACTION_DELETE:

                $this->sql = "DELETE FROM {$this->table}{$joinString}{$whereString}{$havingString}";

                break;

        }

        $this->sql = str_replace('  ', ' ', trim($this->sql));

        if ($prepare) {
            $this->prepareStatement = $this->prepare($this->sql);
        }

        return $this->sql;
    }

}
