<?php

/**
 * ORMSchemeCreator.php
 */
namespace PiecesPHP\Core\Database\ORM;

use JsonSerializable;
use PiecesPHP\Core\Database\Database;
use PiecesPHP\Core\Database\ORM\Fields\Field;
use PiecesPHP\Core\Database\ORM\Fields\ForeignKey;
use PiecesPHP\Core\Database\ORM\Fields\SQLTypesEnum;
use TypeError;

/**
 * ORMSchemeCreator
 *
 * @package     PiecesPHP\Core\Database\ORM
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2020
 */
class ORMSchemeCreator implements JsonSerializable
{

    /**
     * @var string[]
     */
    protected $typesCollations = SQLTypesEnum::STRINGS;

    /**
     * @var array
     */
    protected $typesLengths = [
        SQLTypesEnum::TYPE_VARCHAR => 255,
    ];

    /**
     * @var ActiveRecord
     */
    private $model = null;

    /**
     * @var ORM
     */
    private $mapper = null;

    /**
     * @var string[]
     */
    private $sqlParts = [];

    /**
     * @var bool
     */
    protected $dropIfExist = false;

    /**
     * @var bool
     */
    protected $ignoreFK = false;

    /**
     * @var ORMSchemeCreator[]
     */
    protected static $group = [];

    /**
     * @var Database
     */
    protected static $database = null;

    /**
     * @param ORM $mapper
     * @param bool $primaryKeyAlwaysAutoIncrement
     * @param bool $dropIfExist
     * @param bool $ignoreFK
     */
    public function __construct(ORM $mapper, bool $primaryKeyAlwaysAutoIncrement = true, bool $dropIfExist = false, bool $ignoreFK = false)
    {
        $this->mapper = $mapper;
        $this->dropIfExist = $dropIfExist;
        $this->ignoreFK = $ignoreFK;
        $this->model = $mapper->getModel();

        $fields = $mapper->getFields(true);
        $table = $this->model->getTable();

        $keysSQL = [];
        $fieldsSQL = [];

        $this->sqlParts[] = "CREATE TABLE IF NOT EXISTS `{$table}`(";

        $i = 0;

        /**
         * @var string $fieldName
         * @var Field $field
         */
        foreach ($fields as $fieldName => $field) {

            $type = $field->getType();
            /** @var int */
            $length = $field->sqlLength();
            $nullable = $field->sqlNullable();
            $isPrimaryKey = $field->sqlIsPrimaryKey();
            $isAutoIncrement = $field->sqlAutoIncrement();
            /** @var ForeignKey|null */
            $foreign = $field->foreignKey();
            $referenceTableFk = $foreign !== null ? $foreign->getTable() : null;
            $referenceFieldFk = $foreign !== null ? $foreign->getName() : null;

            if ($isPrimaryKey === true && $primaryKeyAlwaysAutoIncrement) {

                $isAutoIncrement = true;

            }

            $fieldsSQL[$i] = [];

            $fieldsSQL[$i][] = "`{$fieldName}`";

            $type = array_key_exists($type, SQLTypesEnum::TYPES) ? SQLTypesEnum::TYPES[$type] : $type;

            if ($length < 1) {

                $length = array_key_exists($type, $this->typesLengths) ? $this->typesLengths[$type] : null;

            }

            if (!is_null($length)) {

                $fieldsSQL[$i][] = "{$type}($length)";

            } else {

                $fieldsSQL[$i][] = "{$type}";

            }

            $withCollate = array_key_exists($type, $this->typesCollations);

            if ($nullable !== true) {

                if ($withCollate) {

                    $fieldsSQL[$i][] = "COLLATE utf8_bin NOT NULL";

                } else {

                    $fieldsSQL[$i][] = "NOT NULL";

                }

            } else {

                if ($withCollate) {

                    $fieldsSQL[$i][] = "COLLATE utf8_bin NOT NULL";

                }

            }

            if ($isAutoIncrement === true) {

                $fieldsSQL[$i][] = "AUTO_INCREMENT";

            }

            if ($isPrimaryKey === true) {

                $keysSQL[] = "\tPRIMARY KEY (`{$fieldName}`)";

            }

            if (is_string($referenceTableFk) && is_string($referenceFieldFk)) {

                $keysSQL[] = "\tFOREIGN KEY (`{$fieldName}`) REFERENCES {$referenceTableFk}(`{$referenceFieldFk}`)";

            }

            $fieldsSQL[$i] = "\r\n\t" . trim(implode(' ', $fieldsSQL[$i]));

            $i++;

        }

        $this->sqlParts[] = implode(", ", $fieldsSQL) . ',';
        $this->sqlParts[] = "\r\n" . implode(", \r\n", $keysSQL);
        $this->sqlParts[] = "\r\n) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;";

    }

    /**
     * @param ORM[] $group
     * @return static[]
     */
    public static function prepareGroup(array $group)
    {

        self::$group = [];

        foreach ($group as $key => $orm) {

            if ($orm instanceof ORM) {
                self::$group[$key] = new static($orm);
                if (self::$database === null) {
                    self::setDatabase($orm->getDatabase());
                }
            } else {
                throw new TypeError('$group debe ser ORM[].');
            }

        }

        return self::$group;

    }

    /**
     * @param bool $dropIfExist
     * @param bool $ignoreFK
     * @return string
     */
    public static function getGroupSQL(bool $dropIfExist = false, bool $ignoreFK = false)
    {
        $sql = [];

        foreach (self::$group as $creator) {
            $sql[] = $creator->getSQL($dropIfExist);
        }

        if ($ignoreFK) {
            array_unshift($sql, "SET FOREIGN_KEY_CHECKS=0;\r\n");
            array_push($sql, "\r\nSET FOREIGN_KEY_CHECKS=1;");
        }

        return implode("\r\n", $sql);

    }

    /**
     * @param bool $dropIfExist
     * @param bool $ignoreFK
     * @return bool
     */
    public static function groupCreate(bool $dropIfExist = false, bool $ignoreFK = false)
    {
        $sql = self::getGroupSQL($dropIfExist, $ignoreFK);
        $pdo = self::$database->prepare($sql);
        return $pdo->execute();
    }

    /**
     * @param bool $dropIfExist
     * @return bool
     */
    public function create(bool $dropIfExist = false)
    {

        $pdo = $this->model->prepare($this->getSQL($dropIfExist));
        return $pdo->execute();

    }

    /**
     * @param bool $dropIfExist
     * @param bool $ignoreFK
     * @return string
     */
    public function getSQL(bool $dropIfExist = false, bool $ignoreFK = false)
    {

        $sql = $this->sqlParts;

        if (($dropIfExist && $this->dropIfExist) || $dropIfExist) {
            array_unshift($sql, "DROP TABLE IF EXISTS `{$this->mapper->getTable()}`;\r\n");
        }

        if (($ignoreFK && $this->ignoreFK) || $ignoreFK) {
            array_unshift($sql, "SET FOREIGN_KEY_CHECKS=0;\r\n");
            array_push($sql, "\r\nSET FOREIGN_KEY_CHECKS=1;");
        }

        return implode('', $sql);

    }

    /**
     * @param Database $database
     * @return void
     */
    public static function setDatabase(Database $database)
    {
        self::$database = $database;
    }

    /**
     * @return array
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
        return [
            'mapper' => $this->mapper->humanReadable(),
            'sql' => $this->getSQL(),
        ];
    }

}
