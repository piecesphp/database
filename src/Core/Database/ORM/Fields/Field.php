<?php
/**
 * Field.php
 */
namespace PiecesPHP\Core\Database\ORM\Fields;

/**
 * Field.
 *
 * @package     PiecesPHP\Core\Database\ORM\Fields
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2020
 */
class Field
{

    /**
     * @var string
     */
    protected $name = null;
    /**
     * @var mixed
     */
    protected $defaultValue = null;
    /**
     * @var mixed
     */
    protected $value = null;
    /**
     * @var callable
     */
    protected $phpValidator = null;
    /**
     * @var callable
     */
    protected $sqlValidator = null;
    /**
     * @var callable
     */
    protected $phpParser = null;
    /**
     * @var callable
     */
    protected $sqlParser = null;
    /**
     * @var bool
     */
    protected $sqlAutoIncrement = false;
    /**
     * @var bool
     */
    protected $sqlIsPrimaryKey = false;
    /**
     * @var ForeignKey|null
     */
    protected $foreignKey = null;
    /**
     * @var string
     */
    protected $sqlType = SQLTypesEnum::TYPE_TEXT;
    /**
     * @var int Si se define 0 será indefinido
     */
    protected $sqlLength = 0;
    /**
     * @var bool Si es llave primaria será false
     */
    protected $sqlNullable = false;
    /**
     * @var string
     */
    protected $typeForParse = SQLTypesEnum::TYPE_TEXT;

    /**
     * @param string $name
     * @param string $type
     * @param mixed $defaultValue
     * @param ForeignKey $foreign
     */
    public function __construct(string $name, string $type = SQLTypesEnum::TYPE_TEXT, $defaultValue = null, ForeignKey $foreign = null)
    {
        $this->name = trim($name);
        $this->defaultValue = $defaultValue;
        $this->foreignKey = $foreign;

        $this->setType($type);

        $this->setSqlParser(function ($value) {

            $typeForParse = $this->typeForParse;
            $foreign = $this->foreignKey;

            if ($foreign === null) {
                $value = DataProcess::castTypes($typeForParse, $value, true);
            } else {

                $foreignField = $foreign->getName();
                $typeForParse = $this->sqlType;
                $orm = $foreign->getORM();

                if ($orm !== null) {

                    if ($value instanceof $orm) {
                        $value = $value->$foreignField;
                    } else {
                        $value = DataProcess::castTypes($typeForParse, $value, true);
                    }

                } else {
                    $value = DataProcess::castTypes($typeForParse, $value, true);
                }

            }

            return $value;

        });

        $this->setPhpParser(function ($value) {

            $typeForParse = $this->typeForParse;
            $foreign = $this->foreignKey;

            if ($foreign === null) {
                return DataProcess::castTypes($typeForParse, $value, false);
            } else {

                $foreignField = $foreign->getName();
                $typeForParse = $this->sqlType;
                $orm = $foreign->getORM();

                if ($orm !== null) {

                    if ($value instanceof $orm) {
                        return $value;
                    } else {
                        return call_user_func(get_class($orm) . '::getInstance', $value, $foreignField);
                    }

                } else {
                    return DataProcess::castTypes($typeForParse, $value, false);
                }

            }

        });

        $this->setSqlValidator(function ($value) {

            if ($this->sqlNullable && $value === null) {
                return true;
            } else {

                $typeForParse = $this->typeForParse;
                $foreign = $this->foreignKey;

                if ($foreign === null) {
                    return DataProcess::isValidToCast($typeForParse, $value);
                } else {

                    $foreignField = $foreign->getName();
                    $orm = $foreign->getORM();

                    if ($orm !== null) {

                        if ($value instanceof $orm) {
                            return DataProcess::isValidToCast($typeForParse, $value->$foreignField);
                        } else {
                            return DataProcess::isValidToCast($typeForParse, $value);
                        }

                    } else {
                        return DataProcess::isValidToCast($typeForParse, $value);
                    }

                }

            }

        });

        $this->setPhpValidator(function ($value) {

            if ($this->sqlNullable && $value === null) {
                return true;
            } else {

                $typeForParse = $this->typeForParse;
                $foreign = $this->foreignKey;

                if ($foreign === null) {
                    return DataProcess::isValidToCast($typeForParse, $value);
                } else {

                    $foreignField = $foreign->getName();
                    $orm = $foreign->getORM();

                    if ($orm !== null) {

                        if ($value instanceof $orm) {
                            return DataProcess::isValidToCast($typeForParse, $value->$foreignField);
                        } else {
                            return DataProcess::isValidToCast($typeForParse, $value);
                        }

                    } else {
                        return DataProcess::isValidToCast($typeForParse, $value);
                    }

                }

            }

        });

    }

    /**
     * @param bool $sqlAutoIncrement
     * @return bool|static
     */
    public function sqlAutoIncrement(bool $sqlAutoIncrement = null)
    {

        if ($sqlAutoIncrement !== null) {
            $this->sqlAutoIncrement = $sqlAutoIncrement;
            return $this;
        } else {
            return $this->sqlAutoIncrement;
        }

    }

    /**
     * @param bool $sqlIsPrimaryKey
     * @return bool|static
     */
    public function sqlIsPrimaryKey(bool $sqlIsPrimaryKey = null)
    {

        if ($sqlIsPrimaryKey !== null) {
            $this->sqlIsPrimaryKey = $sqlIsPrimaryKey;
            return $this;
        } else {
            return $this->sqlIsPrimaryKey;
        }

    }

    /**
     * @param ForeignKey $foreignKey
     * @return ForeignKey|null|static
     */
    public function foreignKey(ForeignKey $foreignKey = null)
    {

        if ($foreignKey !== null) {
            $this->foreignKey = $foreignKey;
            return $this;
        } else {
            return $this->foreignKey;
        }

    }

    /**
     * @param int $sqlLength
     * @return int|static
     */
    public function sqlLength(int $sqlLength = null)
    {

        if ($sqlLength !== null) {
            $this->sqlLength = $sqlLength;
            return $this;
        } else {
            return $this->sqlLength;
        }

    }

    /**
     * @param bool $sqlNullable
     * @return bool|static
     */
    public function sqlNullable(bool $sqlNullable = null)
    {

        if ($sqlNullable !== null) {
            $this->sqlNullable = $sqlNullable;
            return $this;
        } else {
            return $this->sqlNullable;
        }

    }

    /**
     * @return string
     */
    public function getTypeForParse()
    {
        return $this->typeForParse;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->sqlType;
    }

    /**
     * @param string $type
     * @return static
     * @throws \Exception
     */
    public function setType(string $type)
    {
        $type = mb_strtolower($type);
        $isUtilType = in_array($type, SQLTypesEnum::UTILS_TYPES);
        if ($isUtilType) {
            $this->typeForParse = $type;
            $type = SQLTypesEnum::EQUIVALENCES_TYPES[$type];
        } else {
            $this->typeForParse = SQLTypesEnum::getType($type);
        }
        $this->sqlType = SQLTypesEnum::getType($type);
        return $this;
    }

    /**
     * @param mixed $value
     * @return static
     * @throws \Exception
     */
    public function setValue($value)
    {

        if ($value === null) {
            if (!$this->sqlNullable) {
                throw new \Exception("El campo {$this->name} no puede ser nulo.");
            }
        }

        if ($this->validate($value)) {
            $this->value = $this->parse($value);
        } else {
            throw new \Exception("El valor ingresado en {$this->name} no es válido.");
        }

        return $this;

    }

    /**
     * @param callable $validator
     * @return static
     */
    protected function setPhpValidator(callable $validator)
    {
        $this->phpValidator = $validator;
        return $this;
    }

    /**
     * @param callable $validator
     * @return static
     */
    protected function setSqlValidator(callable $validator)
    {
        $this->sqlValidator = $validator;
        return $this;
    }

    /**
     * @param callable $parser
     * @return static
     */
    protected function setPhpParser(callable $parser)
    {
        $this->phpParser = $parser;
        return $this;
    }

    /**
     * @param callable $parser
     * @return static
     */
    protected function setSqlParser(callable $parser)
    {
        $this->sqlParser = $parser;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPHPValue()
    {
        return $this->phpParse($this->value);
    }

    /**
     * @return mixed
     */
    public function getSQLValue()
    {
        return $this->sqlParse($this->value);
    }

    /**
     * @return mixed
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $value
     * @return bool
     */
    public function validate($value)
    {
        return $this->sqlValidate($value) || $this->phpValidate($value);
    }

    /**
     * @param mixed $value
     * @return bool
     */
    public function sqlValidate($value)
    {
        return ($this->sqlNullable && $value === null) || ($this->sqlValidator)($value);
    }

    /**
     * @param mixed $value
     * @return bool
     */
    public function phpValidate($value)
    {
        return ($this->sqlNullable && $value === null) || ($this->phpValidator)($value);
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    public function parse($value)
    {
        if ($this->sqlValidate($value)) {
            $value = $this->sqlParse($value);
        } elseif ($this->phpValidate($value)) {
            $value = $this->phpParse($value);
        }
        return $value;
    }

    /**
     * @param mixed $value
     * @return bool
     */
    public function sqlParse($value)
    {
        return $this->sqlValidate($value) ? ($this->sqlParser)($value) : $value;
    }

    /**
     * @param mixed $value
     * @return bool
     */
    public function phpParse($value)
    {
        return $this->phpValidate($value) ? ($this->phpParser)($value) : $value;
    }

}
