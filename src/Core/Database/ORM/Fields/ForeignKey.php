<?php
/**
 * ForeignKey.php
 */
namespace PiecesPHP\Core\Database\ORM\Fields;

use PiecesPHP\Core\Database\ORM\ORM;

/**
 * ForeignKey.
 *
 * @package     PiecesPHP\Core\Database\ORM\Fields
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2020
 */
class ForeignKey
{
    /**
     * @var string
     */
    protected $table = null;
    /**
     * @var string
     */
    protected $name = null;
    /**
     * @var bool
     */
    protected $many = false;
    /**
     * @var ORM|null
     */
    protected $orm = null;

    /**
     * @param string $table
     * @param string $name
     * @param bool $many
     * @param ORM $orm
     */
    public function __construct(string $table, string $name, bool $many = false, ORM $orm = null)
    {
        $this->table = $table;
        $this->name = $name;
        $this->many = $many;
        $this->orm = $orm;
    }

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function getMany()
    {
        return $this->many;
    }

    /**
     * @param ORM $orm
     * @return static
     */
    public function setORM(ORM $orm = null)
    {
        $this->orm = $orm;
        return $this;
    }

    /**
     * @return ORM|null
     */
    public function getORM()
    {
        return $this->orm;
    }

}
