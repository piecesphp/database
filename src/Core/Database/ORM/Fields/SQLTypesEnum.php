<?php
/**
 * SQLTypesEnum.php
 */
namespace PiecesPHP\Core\Database\ORM\Fields;

use Exception;

/**
 * SQLTypesEnum.
 *
 * @package     PiecesPHP\Core\Database\ORM\Fields
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2020
 */
class SQLTypesEnum
{

    const UTIL_TYPE_SERIALIZED = 'serialized_object';
    const UTIL_TYPE_JSON = 'json';
    const UTIL_TYPE_ARRAY = 'array';

    const TYPE_VARCHAR = 'varchar';
    const TYPE_TEXT = 'text';
    const TYPE_MEDIUM_TEXT = 'mediumtext';
    const TYPE_LONG_TEXT = 'longtext';

    const TYPE_INT = 'int';
    const TYPE_BIGINT = 'bigint';

    const TYPE_FLOAT = 'float';
    const TYPE_DOUBLE = 'double';

    const TYPE_DATE = 'datetime';
    const TYPE_DATETIME = 'date';

    const STRINGS = [
        self::TYPE_VARCHAR,
        self::TYPE_TEXT,
        self::TYPE_MEDIUM_TEXT,
        self::TYPE_LONG_TEXT,
    ];

    const INTEGERS = [
        self::TYPE_INT,
        self::TYPE_BIGINT,
    ];

    const DECIMALS = [
        self::TYPE_FLOAT,
        self::TYPE_DOUBLE,
    ];

    const DATES = [
        self::TYPE_DATE,
        self::TYPE_DATETIME,
    ];

    const UTILS_TYPES = [
        self::UTIL_TYPE_SERIALIZED,
        self::UTIL_TYPE_JSON,
        self::UTIL_TYPE_ARRAY,
    ];

    const TYPES = [
        self::TYPE_VARCHAR,
        self::TYPE_TEXT,
        self::TYPE_MEDIUM_TEXT,
        self::TYPE_LONG_TEXT,

        self::TYPE_INT,
        self::TYPE_BIGINT,

        self::TYPE_FLOAT,
        self::TYPE_DOUBLE,

        self::TYPE_DATE,
        self::TYPE_DATETIME,
    ];

    const EQUIVALENCES_TYPES = [
        self::UTIL_TYPE_SERIALIZED => self::TYPE_LONG_TEXT,
        self::UTIL_TYPE_JSON => self::TYPE_LONG_TEXT,
        self::UTIL_TYPE_ARRAY => self::TYPE_LONG_TEXT,

        self::TYPE_VARCHAR => self::TYPE_VARCHAR,
        self::TYPE_TEXT => self::TYPE_TEXT,
        self::TYPE_MEDIUM_TEXT => self::TYPE_MEDIUM_TEXT,
        self::TYPE_LONG_TEXT => self::TYPE_LONG_TEXT,

        self::TYPE_INT => self::TYPE_INT,
        self::TYPE_BIGINT => self::TYPE_BIGINT,

        self::TYPE_FLOAT => self::TYPE_FLOAT,
        self::TYPE_DOUBLE => self::TYPE_DOUBLE,

        self::TYPE_DATE => self::TYPE_DATE,
        self::TYPE_DATETIME => self::TYPE_DATETIME,
    ];

    /**
     * @param string $type
     * @return string
     * @throws Exception
     */
    public static function getType(string $type)
    {

        $types = self::TYPES;
        $type = mb_strtolower($type);

        if (in_array($type, $types)) {
            return $types[array_search($type, $types)];
        }

        $utilsTypes = self::UTILS_TYPES;

        if (in_array($type, $utilsTypes)) {
            return $utilsTypes[array_search($type, $utilsTypes)];
        }

        throw new Exception("No existe el tipo {$type}");

    }
}
