<?php
/**
 * DataProcess.php
 */
namespace PiecesPHP\Core\Database\ORM\Fields;

use DateTime;
use Exception;

/**
 * DataProcess.
 *
 * @package     PiecesPHP\Core\Database\ORM\Fields
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2020
 */
class DataProcess
{

    /**
     * @param string $typeSQL
     * @param mixed $value
     * @return bool
     */
    public static function isValidToCast(string $typeSQL, $value)
    {

        $typeSQL = SQLTypesEnum::getType($typeSQL);

        $strings = SQLTypesEnum::STRINGS;

        if (in_array($typeSQL, $strings)) {
            $value = self::isValidStringToCast($value);
        }

        $integers = SQLTypesEnum::INTEGERS;

        if (in_array($typeSQL, $integers)) {
            $value = self::isValidIntegerToCast($value);
        }

        $decimals = SQLTypesEnum::DECIMALS;

        if (in_array($typeSQL, $decimals)) {
            $value = self::isValidDecimalToCast($value);
        }

        $dates = SQLTypesEnum::DATES;

        if (in_array($typeSQL, $dates)) {
            $value = self::isValidDateToCast($value);
        }

        if ($typeSQL === SQLTypesEnum::UTIL_TYPE_JSON) {
            $value = self::isValidJsonToCast($value);
        }

        if ($typeSQL === SQLTypesEnum::UTIL_TYPE_SERIALIZED) {
            $value = self::isValidSerializedObject($value);
        }

        if ($typeSQL === SQLTypesEnum::UTIL_TYPE_ARRAY) {
            $value = self::isValidArrayToCast($value);
        }

        return $value;

    }

    /**
     * @param string $typeSQL
     * @param mixed $value
     * @param bool $toSQL
     * @return mixed Si no puede hacer nada con el valor ingresado devuelve el mismo valor
     */
    public static function castTypes(string $typeSQL, $value, bool $toSQL = false)
    {

        $typeSQL = SQLTypesEnum::getType($typeSQL);

        $strings = SQLTypesEnum::STRINGS;

        if (in_array($typeSQL, $strings)) {
            $value = self::stringParse($value, $toSQL);
        }

        $integers = SQLTypesEnum::INTEGERS;

        if (in_array($typeSQL, $integers)) {
            $value = self::integerParse($value, $toSQL);
        }

        $decimals = SQLTypesEnum::DECIMALS;

        if (in_array($typeSQL, $decimals)) {
            $value = self::decimalParse($value, $toSQL);
        }

        $dates = SQLTypesEnum::DATES;

        if (in_array($typeSQL, $dates)) {
            $value = self::dateParse($value, $typeSQL, $toSQL);
        }

        if ($typeSQL === SQLTypesEnum::UTIL_TYPE_JSON) {
            $value = self::jsonParse($value, $toSQL);
        }

        if ($typeSQL === SQLTypesEnum::UTIL_TYPE_SERIALIZED) {
            $value = self::serializedObjectParse($value, $toSQL);
        }

        if ($typeSQL === SQLTypesEnum::UTIL_TYPE_ARRAY) {
            $value = self::arrayParse($value, $toSQL);
        }

        return $value;

    }

    /**
     * @param mixed $value
     * @param bool $toSQL
     * @return string
     */
    public static function stringParse($value, bool $toSQL = true)
    {

        if ($toSQL) {

            if (is_scalar($value)) {

                $value = (string) $value;
                $value = \stripslashes($value);
                $value = \addslashes($value);

            } else {

                if (is_object($value)) {

                    $has_to_string = method_exists(get_class($value), '__toString');

                    if ($has_to_string) {
                        $value = (string) $value;
                        $value = \stripslashes($value);
                        return \addslashes($value);
                    }

                }

                $value = $value;

            }

        } else {
            $value = is_string($value) ? \stripslashes($value) : $value;
        }

        return $value;

    }

    /**
     * @param string|int|double $value
     * @param bool $toSQL
     * @return int
     */
    public static function integerParse($value, bool $toSQL = true)
    {

        if ($toSQL) {

            $valid = true;
            if (!is_int($value)) {
                if (is_string($value)) {
                    $valid = (is_string($value) && ctype_digit($value)) || is_numeric($value);
                } else {
                    $valid = is_numeric($value);
                }
            }

            if ($valid) {
                $value = (int) $value;
            }

        } else {
            $value = (int) $value;
        }

        return $value;

    }

    /**
     * @param string|int|double $value
     * @param bool $toSQL
     * @return double
     */
    public static function decimalParse($value, bool $toSQL = true)
    {

        if ($toSQL) {

            if (is_int($value) || is_float($value) || is_numeric($value)) {
                $value = (double) $value;
            }

        } else {
            $value = (double) $value;
        }

        return $value;

    }

    /**
     * @param mixed $value
     * @param string $type SQLTypesEnum::TYPE_DATETIME|SQLTypesEnum::TYPE_DATE
     * @param bool $toSQL
     * @return string|DateTime
     */
    public static function dateParse($value, string $type, bool $toSQL = true)
    {

        if ($toSQL) {

            if ($type === SQLTypesEnum::TYPE_DATETIME) {
                if ($value instanceof DateTime) {
                    $value = $value->format('Y-m-d H:i:s');
                } else {
                    $value = $value;
                }

            } elseif ($type === SQLTypesEnum::TYPE_DATE) {

                if ($value instanceof DateTime) {
                    return $value->format('Y-m-d');
                } else {
                    return $value;
                }

            }

        } else {

            if ($type === SQLTypesEnum::TYPE_DATETIME || $type === SQLTypesEnum::TYPE_DATE) {

                if (!is_null($value)) {
                    if (is_string($value)) {
                        if (strlen(trim($value)) > 0) {
                            try {
                                $value = new DateTime($value);
                            } catch (Exception $e) {
                                return $value;
                            }
                        }
                    }
                }

            }

        }

        return $value;

    }

    /**
     * @param mixed $value
     * @param bool $toSQL
     * @return string|\stdClass|array
     */
    public static function jsonParse($value, bool $toSQL = true)
    {

        if ($toSQL) {

            if (!is_string($value) && self::isValidJsonToCast($value)) {
                $value = $value !== null ? json_encode($value) : null;
            }

        } else {
            if (self::isValidJsonToCast($value)) {
                $value = $value !== null ? json_decode($value) : null;
            }
        }

        return $value;

    }

    /**
     * @param mixed $value
     * @param bool $toSQL
     * @return string|object
     */
    public static function serializedObjectParse($value, bool $toSQL = true)
    {

        if ($toSQL) {

            if (is_object($value) && self::isValidSerializedObject($value)) {

                $value = [
                    'classname' => get_class($value),
                    'serialized' => serialize($value),
                ];

                $value = json_encode($value);

            }

        } else {

            if (is_string($value) && self::isValidJsonToCast($value) && is_array(json_decode($value, true))) {

                $decoded = json_decode($value, true);
                $classname = isset($decoded['classname']) ? $decoded['classname'] : null;
                $serialized = isset($decoded['serialized']) ? $decoded['serialized'] : null;

                if (is_string($classname) && is_string($serialized)) {
                    $value = unserialize($serialized);
                }

            }

        }

        return $value;

    }

    /**
     * @param mixed $value
     * @param bool $toSQL
     * @return string|\stdClass|array
     */
    public static function arrayParse($value, bool $toSQL = true)
    {

        if ($toSQL) {

            if (!is_string($value) && self::isValidJsonToCast($value)) {
                $value = json_encode((array) $value);
            }

        } else {
            if (self::isValidJsonToCast($value)) {
                $value = json_decode($value, true);
            }
        }

        return $value;

    }

    /**
     * @param mixed $value
     * @return bool
     */
    public static function isValidStringToCast($value)
    {
        return is_scalar($value);
    }

    /**
     * @param string|int|double $value
     * @return bool
     */
    public static function isValidNumberToCast($value)
    {
        $valueAlt = $value;

        if (is_string($valueAlt) && strlen($valueAlt) > 1) {
            $valueAlt = trim($valueAlt);
            if ($valueAlt[0] == '-') {
                $valueAlt = substr($valueAlt, 1);
            }
        }

        return is_numeric($value) || is_numeric($valueAlt);
    }

    /**
     * @param mixed $value
     * @return bool
     */
    public static function isValidIntegerToCast($value)
    {
        if (is_string($value) && strlen($value) > 1) {
            $value = trim($value);
            if ($value[0] == '-') {
                $value = substr($value, 1);
            }
        }
        return is_scalar($value) && (is_integer($value) || (is_string($value) && ctype_digit($value)));
    }

    /**
     * @param mixed $value
     * @return bool
     */
    public static function isValidArrayToCast($value)
    {
        if (self::isValidJsonToCast($value)) {

            if (is_string($value)) {
                $valueParsed = @json_decode($value, true);
                return is_array($valueParsed);

            } else {
                $valueParsed = @json_encode($value);
                $valueParsed = is_string($valueParsed) ? @json_decode($valueParsed, true) : null;
                return is_array($valueParsed);
            }
        }
        return is_array($value);
    }

    /**
     * @param mixed $value
     * @return bool
     */
    public static function isValidDecimalToCast($value)
    {
        if (is_string($value) && strlen($value) > 1) {
            $value = trim($value);
            if ($value[0] == '-') {
                $value = substr($value, 1);
            }
        }
        return is_float($value) || is_numeric($value);
    }

    /**
     * @param mixed $value
     * @return bool
     */
    public static function isValidBool($value)
    {
        return $value === false || $value === true;
    }

    /**
     * @param mixed $value
     * @return bool
     */
    public static function isValidNull($value)
    {
        return $value === null;
    }

    /**
     * @param mixed $value
     * @return bool
     */
    public static function isValidDateToCast($value)
    {
        if ($value instanceof DateTime) {
            return true;
        } else {
            try {
                (new DateTime($value));
                return true;
            } catch (\Exception $e) {
                return false;
            }
        }
    }

    /**
     * @param mixed $value
     * @return bool
     */
    public static function isValidJsonToCast($value)
    {
        if (is_array($value) || is_object($value)) {
            return true;
        } else {
            $test_encoding = @json_encode($value);
            return json_last_error() == \JSON_ERROR_NONE;
        }
    }

    /**
     * @param mixed $value
     * @return bool
     */
    public static function isValidSerializedObject($value)
    {
        if (is_object($value)) {

            return true;

        } elseif (is_string($value)) {

            if (self::isValidJsonToCast($value)) {

                $decode = json_decode($value, true);
                $classname = isset($decode['classname']) ? $decode['classname'] : null;
                $serialized = isset($decode['serialized']) ? $decode['serialized'] : null;

                if (is_string($classname) && is_string($serialized)) {
                    return class_exists($classname);
                }

            } else {
                return false;
            }

        }

        return false;
    }

}
