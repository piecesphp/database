<?php

/**
 * ORM.php
 */
namespace PiecesPHP\Core\Database\ORM;

use Exception;
use JsonSerializable;
use PiecesPHP\Core\Database\Database;
use PiecesPHP\Core\Database\ORM\Collections\FieldCollection;
use PiecesPHP\Core\Database\ORM\Fields\Field;
use PiecesPHP\Core\Database\ORM\Fields\ForeignKey;
use PiecesPHP\Core\Database\ORM\Fields\SQLTypesEnum;

/**
 * ORM
 *
 * @package     PiecesPHP\Core\Database\ORM
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2020
 */
class ORM implements JsonSerializable
{

    /**
     * @var Database|null
     */
    protected static $staticDatabase = null;
    /**
     * @var string
     */
    protected static $staticLastSQLPrepared = "";
    /**
     * @var Database Si no es definidad directamente tomará el valor de la definida en la primera instancia
     */
    protected $database = null;
    /**
     * @var ActiveRecord
     */
    protected $model = null;
    /**
     * @var string
     */
    protected $table = 'somethingTable';
    /**
     * @var FieldCollection
     */
    protected $fields = null;
    /**
     * @var string
     */
    protected $lastSQLPrepared = "";
    /**
     * @var string
     */
    protected $fieldPrimaryKeyName = null;

    /**
     * Instanciará el ORM con los valores de la primera coindencia del criterio provisto
     *
     * @param mixed $findValue
     * @param string $column
     * @param Database $database Si no se proporcioná se intentará usar  self::$staticDatabase
     */
    public function __construct($findValue = null, string $column = 'id', Database $database = null)
    {
        if ($database === null) {
            $database = self::$staticDatabase;
        }

        if ($database === null) {
            throw new Exception('La base de datos debe ser configurada.');
        } else {

            $this->database = $database;

            if (self::$staticDatabase === null) {
                self::$staticDatabase = $this->database;
            }

        }

        $this->validateFields();

        if ($this->fieldPrimaryKeyName === null) {
            throw new Exception('Debe haber una clave primaria.');
        }

        $this->model = new ActiveRecord($database, $this->table);
        $this->model->setFields($this->fields->getNames());

        if ($findValue !== null) {

            if ($column === 'primary_key') {
                $column = $this->fieldPrimaryKeyName;
            }

            $record = $this->searchOneBy($column, $findValue);

            if ($record !== null) {

                $properties = array_keys(get_object_vars($record));

                foreach ($properties as $property) {

                    $this->$property = $record->$property;

                }

            }

        }

    }

    /**
     * @param mixed $findValue
     * @param string $column
     * @param bool $recursiveORM
     * @return static
     */
    public static function getInstance($findValue = null, string $column = 'id', bool $recursiveORM = true)
    {

        $instance = new static;

        if (!$recursiveORM) {
            foreach ($instance->getInstanceFields(true) as $field) {

                if (!is_string($field)) {
                    /** @var ForeignKey|null */
                    $foreign = $field->foreignKey();
                    if ($foreign !== null) {
                        $foreign->setORM(null);
                    }
                }

            }
        }

        if ($findValue !== null) {

            if ($column === 'primary_key') {
                $column = $instance->fieldPrimaryKeyName;
            }

            $record = $instance->searchOneBy($column, $findValue);

            if ($record !== null) {

                $properties = array_keys(get_object_vars($record));

                foreach ($properties as $property) {

                    $instance->$property = $record->$property;

                }

            }

        }

        return $instance;

    }

    /**
     * @return ActiveRecord
     */
    public function prepareModelForSave()
    {

        $data = [];

        foreach ($this->fields->getNames() as $fieldName) {

            $field = $this->fields->getByName($fieldName);

            if ($field !== null) {
                $primaryKey = $field->sqlIsPrimaryKey();
                $default = $field->getDefaultValue();
                $nullable = $field->sqlNullable();

                if (!$nullable) {

                    if ($field->getPHPValue() === null) {
                        if ($primaryKey !== true) {
                            $field->setValue($default);
                        }
                    }

                    if ($field->getPHPValue() !== null) {
                        $data["`{$fieldName}`"] = $field->getSQLValue();
                    }

                } else {
                    $data["`{$fieldName}`"] = $field->getSQLValue();
                }
            }

        }

        $fieldPrimaryKeyName = $this->fieldPrimaryKeyName;
        unset($data["`{$fieldPrimaryKeyName}`"]);

        if ($this->$fieldPrimaryKeyName !== null) {
            return $this->prepareModelForUpdate();
        } else {
            return $this->model->insert($data);
        }

    }

    /**
     * @return ActiveRecord
     */
    public function prepareModelForUpdate()
    {

        $data = [];
        $primaryKeyName = '';

        foreach ($this->fields->getNames() as $fieldName) {

            $field = $this->fields->getByName($fieldName);

            if ($field !== null) {
                $primaryKey = $field->sqlIsPrimaryKey();
                /** @var ForeignKey|null */
                $foreign = $field->foreignKey();
                $default = $field->getDefaultValue();
                $canBeNull = $field->sqlNullable();
                $phpValue = $field->getPHPValue();

                if ($primaryKey) {
                    $primaryKeyName = $fieldName;
                }

                $validForUpdate = $fieldName != $primaryKeyName; //Que no sea la llave primaria

                if (!$canBeNull) {
                    $validForUpdate = $validForUpdate && $phpValue !== null; //Que no sea nulo
                }

                if ($validForUpdate) {

                    if ($foreign === null) {

                        $data["`{$fieldName}`"] = $field->getSQLValue();

                    } else {

                        $foreignField = $foreign->getName();
                        $orm = $foreign->getORM();
                        $value = $field->getPHPValue();

                        if ($orm !== null && $value instanceof $orm) {

                            $data["`{$fieldName}`"] = $value->$foreignField;

                        } else {

                            $data["`{$fieldName}`"] = $field->getSQLValue();

                        }

                    }

                }
            }

        }

        return $this->model->update($data)->where([$primaryKeyName => $this->$primaryKeyName]);

    }

    /**
     * @return bool
     */
    public function save()
    {
        $model = $this->prepareModelForSave();
        $success = $model->execute();
        $this->lastSQLPrepared = $model->getLastSQLExecuted();
        return $success === true;
    }

    /**
     * @return bool
     */
    public function update()
    {
        $model = $this->prepareModelForUpdate();
        $success = $model->execute();
        $this->lastSQLPrepared = $model->getLastSQLExecuted();
        return $success === true;
    }

    /**
     * @param mixed $name
     * @return mixed
     */
    public function __get($name)
    {

        $this->verifyAccess($name, get_class($this), [
            get_class($this),
            self::class,
        ]);

        if ($this->fields->getByName($name) !== null) {

            return $this->fields->getByName($name)->getPHPValue();

        } else {

            return $this->$name;

        }

    }

    /**
     * @param mixed $name
     * @param mixed $value
     * @return void
     */
    public function __set($name, $value)
    {

        $this->verifyAccess($name, get_class($this), [
            get_class($this),
            self::class,
        ]);

        if ($this->fields->getByName($name) !== null) {

            $this->fields->getByName($name)->setValue($value);

        } else {
            $this->$name = $value;
        }

    }

    /**
     * @return ActiveRecord
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @return int
     */
    public function getLastInsertID()
    {
        return $this->model->lastInsertId();
    }

    /**
     * @param bool $asFieldInstances
     * @return string[]|array<string,Field>
     */
    public function getInstanceFields(bool $asFieldInstances = false)
    {
        /** @var string[] $fieldNames */
        $fieldNames = [];
        /** @var array<string,Field> $fields */
        $fields = [];
        foreach ($this->fields->getNames() as $fieldName) {
            $field = $this->fields->getByName($fieldName);
            if ($field !== null) {
                $fields[$fieldName] = $field;
                $fieldNames[] = $fieldName;
            }
        }

        if ($asFieldInstances) {
            return $fields;
        } else {
            return $fieldNames;
        }

    }

    /**
     * @return string
     */
    public static function getPrimaryKey()
    {
        return (new static())->fieldPrimaryKeyName;
    }

    /**
     * @return string
     */
    public static function getTable()
    {
        $instance = new static();
        return $instance->table;
    }

    /**
     * @param bool $asFieldInstances
     * @return string[]|array<string,Field>
     */
    public static function getFields(bool $asFieldInstances = false)
    {
        /** @var static $instance */
        $instance = new static();
        /** @var string[] $fieldNames */
        $fieldNames = [];
        /** @var array<string,Field> $fields */
        $fields = [];

        foreach ($instance->fields->getNames() as $fieldName) {
            $field = $instance->fields->getByName($fieldName);
            if ($field !== null) {
                $fields[$fieldName] = $field;
                $fieldNames[] = $fieldName;
            }
        }

        if ($asFieldInstances) {
            return $fields;
        } else {
            return $fieldNames;
        }

    }

    /**
     * @return static[]
     */
    public function getAll()
    {
        $result = $this->model->getAll();
        $result = is_array($result) ? $result : [];
        $results = [];

        foreach ($result as $e) {
            $instance = new static();
            foreach ($e as $property => $value) {
                $instance->$property = $value;
            }
            $results[] = $instance;
        }

        return $results;
    }

    /**
     * @return array
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
        $data = [];
        $data['table'] = $this->table;
        $data['fieldPrimaryKeyName'] = $this->fieldPrimaryKeyName;
        $data['fields'] = [];

        foreach ($this->fields->getNames() as $name) {
            $field = $this->fields->getByName($name);
            if ($field !== null) {
                $data['fields'][$name] = $field->getPHPValue();
            }
        }

        return $data;
    }

    /**
     * @return array
     */
    public function humanReadable()
    {
        $data = [];

        foreach ($this->fields->getNames() as $fieldName) {

            $field = $this->fields->getByName($fieldName);

            if ($field !== null) {

                /** @var ForeignKey|null */
                $foreign = $field->foreignKey();

                if ($foreign === null) {

                    $data[$fieldName] = $field->getSQLValue();

                } else {

                    $orm = $foreign->getORM();
                    $value = $field->getPHPValue();

                    if ($orm !== null && $value instanceof $orm) {

                        $data[$fieldName] = $value->humanReadable();

                    } else {

                        $data[$fieldName] = $field->getSQLValue();

                    }

                }

            }

        }

        return $data;
    }

    /**
     * @param string $column
     * @param mixed $value
     * @return \stdClass|null
     */
    public function searchOneBy(string $column, $value)
    {
        $result = self::searchOnTableOneBy($this->table, $column, $value);
        $this->lastSQLPrepared = self::$staticLastSQLPrepared;
        return $result;
    }

    /**
     * @return void
     */
    private function validateFields()
    {

        $hasPrimaryKey = false;

        if (count($this->fields) === 0) {
            throw new Exception('Deben configurarse los campos.');
        } else {

            foreach ($this->fields->getNames() as $fieldName) {

                $field = $this->fields->getByName($fieldName);

                if ($field !== null) {
                    $primaryKey = $field->sqlIsPrimaryKey();
                    $sqlAutoIncrement = $field->sqlAutoIncrement();
                    /** @var int */
                    $length = $field->sqlLength();
                    $acceptNull = $field->sqlNullable();
                    $foreign = $field->foreignKey();
                    $type = $field->getType();
                    $typeForParse = $field->getTypeForParse();
                    $default = $field->getDefaultValue();

                    if ($primaryKey) {

                        if (!$hasPrimaryKey) {
                            $this->fieldPrimaryKeyName = $fieldName;
                            $hasPrimaryKey = true;
                        } else {
                            throw new Exception('Debe haber solo una clave primaria.');
                        }

                    }

                    if ($type === SQLTypesEnum::TYPE_VARCHAR) {

                        if ($length < 1) {
                            throw new Exception('El campo varchar debe tener una longitud definida.');
                        }

                    }
                }

            }

        }

    }

    /**
     * @return Database
     */
    public function getDatabase()
    {
        return $this->database;
    }

    /**
     * @param string $propertyToVerifyName
     * @param string $currentClassName
     * @param string[] $classes
     * @return void
     * @throws Exception
     */
    private function verifyAccess(string $propertyToVerifyName, string $currentClassName, array $classes)
    {

        foreach ($classes as $classname) {

            $reflection = new \ReflectionClass($classname);

            $privateProperties = $reflection->getProperties(\ReflectionProperty::IS_PRIVATE);

            foreach ($privateProperties as $property) {
                if ($property->name == $propertyToVerifyName) {
                    throw new Exception("$currentClassName::$propertyToVerifyName is private");
                }
            }

            $protectedProperties = $reflection->getProperties(\ReflectionProperty::IS_PROTECTED);

            foreach ($protectedProperties as $property) {
                if ($property->name == $propertyToVerifyName) {
                    throw new Exception("$currentClassName::$propertyToVerifyName is protected");
                }
            }

        }

    }

    /**
     * @param string $table
     * @param string $column
     * @param mixed $value
     * @param string[] $fieldsOnSelect
     * @return \stdClass|null
     */
    public static function searchOnTableOneBy(string $table, string $column, $value, array $fieldsOnSelect = ['*'])
    {
        $fieldsOnSelect = implode(', ', $fieldsOnSelect);
        $SQL = "SELECT {$fieldsOnSelect} FROM {$table} WHERE {$column} = :VALUE LIMIT 1";

        self::$staticLastSQLPrepared = $SQL;

        $prepared = self::$staticDatabase !== null ? self::$staticDatabase->prepare(self::$staticLastSQLPrepared) : false;

        if ($prepared !== false) {
            $prepared->execute([
                ':VALUE' => $value,
            ]);
            $result = $prepared->fetchAll(Database::FETCH_OBJ);
        } else {
            $result = [];
        }

        $result = count($result) > 0 ? $result[0] : null;

        return $result;

    }

    /**
     * @param Database $database
     * @return void
     */
    public static function setDatabase(Database $database)
    {
        self::$staticDatabase = $database;
    }

}
