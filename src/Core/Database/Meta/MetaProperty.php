<?php

/**
 * MetaProperty.php
 */
namespace PiecesPHP\Core\Database\Meta;

use DateTime;
use PiecesPHP\Core\Database\ORM\ORM;

/**
 * MetaProperty
 *
 * @package     PiecesPHP\Core\Database\Meta
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2020
 */
class MetaProperty
{

    const TYPE_ARRAY = 'ARRAY';
    const TYPE_TEXT = 'TEXT';
    const TYPE_NUMBER = 'NUMBER';
    const TYPE_INT = 'INT';
    const TYPE_JSON = 'JSON';
    const TYPE_DOUBLE = 'DOUBLE';
    const TYPE_DATE = 'DATE';
    const TYPE_MAPPER = 'MAPPER';
    const TYPE_ARRAY_MAPPER = 'ARRAY_MAPPER';

    const TYPES = [
        self::TYPE_ARRAY,
        self::TYPE_TEXT,
        self::TYPE_NUMBER,
        self::TYPE_INT,
        self::TYPE_JSON,
        self::TYPE_DOUBLE,
        self::TYPE_DATE,
        self::TYPE_MAPPER,
        self::TYPE_ARRAY_MAPPER,
    ];

    /**
     * @var string
     */
    protected $type = self::TYPE_TEXT;

    /**
     * @var string
     */
    protected $mapperName = null;

    /**
     * @var mixed
     */
    protected $value = null;

    /**
     * @var mixed
     */
    protected $defaultValue = null;

    /**
     * @var mixed
     */
    protected $nullable = true;

    /**
     * @var string
     */
    protected $propertyMapper = 'id';

    /**
     * @var callable|null
     */
    protected $customSetMapper = null;

    /**
     * @var callable|null
     */
    protected $customValidMapper = null;

    /**
     * @var callable|null
     */
    protected $customGetValueToSQL = null;

    /**
     * @var bool
     */
    protected $settedValue = false;

    /**
     * @param string $type
     * @param mixed $defaultValue
     * @param bool $nullable
     * @return static
     * @throws \Exception
     */
    public function __construct(string $type = 'TEXT', $defaultValue = null, bool $nullable = true, string $mapperName = null, string $propertyMapper = 'id')
    {

        $this->nullable = $nullable;
        $this->mapperName = $mapperName;
        $this->propertyMapper = $propertyMapper;

        if (in_array($type, self::TYPES)) {

            $this->type = $type;

            if ($this->type == self::TYPE_MAPPER || $this->type == self::TYPE_ARRAY_MAPPER) {

                if (!is_string($mapperName) || strlen(trim($mapperName)) == 0) {

                    throw new \Exception('Debe proveer el nombre de la clase del mapper.');

                }

            }

        } else {

            throw new \Exception('No existe el tipo proporcionado: ' . $type);

        }

        $valid = $this->validateValue($defaultValue);

        if ($this->nullable) {

            if (is_null($defaultValue)) {
                $valid = true;
            }

        }

        if ($valid) {

            if ($this->type == self::TYPE_DATE) {

                if ($defaultValue === 'now') {
                    $defaultValue = date('Y-m-d H:i:s');
                }

            }

            $this->defaultValue = $defaultValue;

        } else {

            throw new \Exception('El valor proporcionado no es compatible con el tipo definido: ' . $type);

        }

    }

    /**
     * @param mixed $value
     * @return static
     * @throws \Exception
     */
    public function setValue($value)
    {

        $this->settedValue = true;

        $valid = true;
        $existsOnMapper = true;

        if ($this->type == self::TYPE_MAPPER) {

            $valid = $this->validateValue($value);
            $existsOnMapper = false;

            if (!$valid) {

                if (is_null($this->customSetMapper)) {

                    $mapper = $this->mapperName;
                    $value = call_user_func($mapper, 'getInstance', $value, $this->propertyMapper);

                } else {

                    $value = ($this->customSetMapper)($value);

                }

            }

            $valid = $this->validateValue($value);

            if ($valid) {

                if (is_null($this->customValidMapper)) {

                    $existsOnMapper = $value->id !== null;

                } else {

                    $existsOnMapper = ($this->customValidMapper)($value) === true;

                }

            }

        } else if ($this->type == self::TYPE_ARRAY_MAPPER) {

            $valid = $this->validateValue($value) && is_array($value);

            if (!$valid) {

                if (is_null($this->customSetMapper)) {

                    $mapper = $this->mapperName;

                    foreach ($value as $k => $v) {

                        $value[$k] = call_user_func($mapper, 'getInstance', $v, $this->propertyMapper);

                    }

                } else {

                    $mapper = $this->mapperName;

                    foreach ($value as $k => $v) {

                        $value[$k] = ($this->customSetMapper)($v);

                    }

                }

            }

            $valid = $this->validateValue($value);

            if ($valid) {

                foreach ($value as $k => $v) {

                    if (is_null($this->customValidMapper)) {

                        if ($v->id === null) {
                            $existsOnMapper = false;
                            break;
                        }

                    } else {

                        if (($this->customValidMapper)($value) !== true) {
                            $existsOnMapper = false;
                            break;
                        }

                    }

                }

            }

        } elseif ($this->type == self::TYPE_DATE) {

            if ($value === 'now') {
                $valid = true;
            } else {
                $valid = $this->validateValue($value);
            }

        } else {

            $valid = $this->validateValue($value);

        }

        if ($this->nullable) {

            if (is_null($value)) {
                $valid = true;
            }

        }

        if ($valid) {

            if ($this->type == self::TYPE_ARRAY) {

                if (!is_null($value)) {
                    $value = (array) $value;
                }

            } elseif ($this->type == self::TYPE_DATE) {

                if ($value === 'now') {
                    $value = new DateTime();
                } else {
                    $value = new DateTime($value);
                }

            }

        }

        if ($valid && $existsOnMapper) {

            $this->value = $value;

        } else {

            if (!$valid) {

                throw new \Exception('El valor proporcionado no es compatible con el campo.');

            } elseif (!$existsOnMapper) {

                if ($this->type == self::TYPE_MAPPER) {
                    throw new \Exception('El valor proporcionado no puede ser encontrado en el mapper.');
                } else {
                    throw new \Exception('Uno o varios de los valores proporcionados no puede ser encontrado en el mapper.');
                }

            }

        }

        return $this;

    }

    /**
     * @return mixed
     */
    public function getValue()
    {

        if ($this->settedValue) {
            return $this->value;
        } else {
            return $this->defaultValue;
        }

    }

    /**
     * @return mixed
     */
    public function getValueHuman()
    {

        $value = $this->value;

        if ($this->type == self::TYPE_MAPPER) {

            if (is_subclass_of($value, ORM::class)) {

                return $this->recursiveHumanization($value);

            } else {
                return $value;
            }

        } else if ($this->type == self::TYPE_ARRAY_MAPPER) {

            if (is_array($value)) {

                foreach ($value as $k => $v) {

                    if (is_subclass_of($v, ORM::class)) {
                        $value[$k] = $this->recursiveHumanization($v);
                    }

                }

                return $value;

            } else {
                return $value;
            }

        } elseif (!is_scalar($value) && !is_null($value)) {
            return (array) $value;
        } else {
            return $value;
        }

    }

    /**
     * @return mixed
     */
    public function getValueToSQL()
    {

        $propertyMapper = $this->propertyMapper;

        if ($this->type == self::TYPE_ARRAY_MAPPER) {

            if ($this->settedValue) {

                $value = $this->value;

                if (is_array($value)) {

                    foreach ($value as $k => $v) {

                        $mapperName = $this->mapperName;
                        $isMapper = $v instanceof $mapperName;

                        if ($isMapper) {

                            $value[$k] = is_null($this->customGetValueToSQL) ? $v->$propertyMapper : ($this->customGetValueToSQL)($v);

                        } else {

                            $value[$k] = $v;
                        }

                    }

                }

                return $value;

            } else {

                $value = $this->defaultValue;

                if (is_array($value)) {

                    foreach ($value as $k => $v) {

                        $mapperName = $this->mapperName;
                        $isMapper = $v instanceof $mapperName;

                        if ($isMapper) {

                            $value[$k] = is_null($this->customGetValueToSQL) ? $v->$propertyMapper : ($this->customGetValueToSQL)($v);

                        } else {

                            $value[$k] = $v;
                        }

                    }

                }

                return $value;

            }

        } elseif ($this->type == self::TYPE_MAPPER) {

            if ($this->settedValue) {

                $isMapper = $this->validateValue($this->value);

                if ($isMapper) {

                    return is_null($this->customGetValueToSQL) ? $this->value->$propertyMapper : ($this->customGetValueToSQL)($this->value);

                } else {

                    return $this->value;
                }

            } else {

                $isMapper = $this->validateValue($this->defaultValue);

                if ($isMapper) {

                    return is_null($this->customGetValueToSQL) ? $this->defaultValue->$propertyMapper : ($this->customGetValueToSQL)($this->defaultValue);

                } else {

                    return $this->defaultValue;
                }

            }

        } else {

            if ($this->settedValue) {
                return $this->value;
            } else {
                return $this->defaultValue;
            }

        }

    }

    /**
     * @param callable $custom
     * @return static
     */
    public function setCustomSetMapper(callable $custom)
    {
        $this->customSetMapper = $custom;
        return $this;
    }

    /**
     * @param callable $custom
     * @return static
     */
    public function setCustomValidMapper(callable $custom)
    {
        $this->customValidMapper = $custom;
        return $this;
    }

    /**
     * @param callable $custom
     * @return static
     */
    public function setCustomGetValueToSQL(callable $custom)
    {
        $this->customGetValueToSQL = $custom;
        return $this;
    }

    /**
     * @param mixed $value
     * @return bool
     */
    public function validateValue($value)
    {

        if ($this->type == self::TYPE_ARRAY) {

            return self::validateType('array', $value);

        } elseif ($this->type == self::TYPE_TEXT) {

            return self::validateType('text', $value);

        } elseif ($this->type == self::TYPE_NUMBER) {

            return self::validateType('number', $value);

        } elseif ($this->type == self::TYPE_INT) {

            return self::validateType('int', $value);

        } elseif ($this->type == self::TYPE_JSON) {

            return self::validateType('json', $value);

        } elseif ($this->type == self::TYPE_DOUBLE) {

            return self::validateType('double', $value);

        } elseif ($this->type == self::TYPE_DATE) {

            return self::validateType('date', $value) || $value === 'now';

        } elseif ($this->type == self::TYPE_MAPPER) {

            return $value instanceof $this->mapperName && is_subclass_of($value, ORM::class);

        } elseif ($this->type == self::TYPE_ARRAY_MAPPER) {

            $valid = self::validateType('array', $value);

            if ($valid) {

                foreach ($value as $v) {

                    $mapperName = $this->mapperName;

                    if (!$v instanceof $mapperName || !is_subclass_of($v, ORM::class)) {

                        $valid = false;
                        break;

                    }

                }

            }

            return $valid;

        } else {
            return false;
        }

    }

    /**
     * @param ORM $mapper
     * @return array
     */
    private function recursiveHumanization(ORM $mapper)
    {

        $dataHuman = $mapper->humanReadable();

        $subFields = array_keys($mapper->getFields());

        foreach ($subFields as $nameSubField) {

            if (is_subclass_of($mapper->$nameSubField, ORM::class)) {

                $dataHuman[$nameSubField] = $this->recursiveHumanization($mapper->$nameSubField);

            }

        }

        return $dataHuman;
    }

    /**
     * @param string $type
     * @param mixed $value
     * @return bool
     */
    public static function validateType(string $type, $value)
    {
        switch ($type) {
            case 'string':
            case 'text':
            case 'varchar':
            case 'mediumtext':
            case 'longtext':
                if (is_scalar($value)) {
                    return true;
                }
                break;
            case 'number':
                if (is_numeric($value)) {
                    return true;
                }
                break;
            case 'int':
                if (is_string($value) && strlen($value) > 1) {
                    $value = trim($value);
                    if ($value[0] == '-') {
                        $value = substr($value, 1);
                    }
                }
                if (is_integer($value) || (is_string($value) && ctype_digit($value))) {
                    return true;
                }
                break;
            case 'array':
                if (is_array($value)) {
                    return true;
                }
                break;
            case 'float':
            case 'double':
                if (is_string($value) && strlen($value) > 1) {
                    $value = trim($value);
                    if ($value[0] == '-') {
                        $value = substr($value, 1);
                    }
                }
                if (is_float($value) || is_numeric($value)) {
                    return true;
                }
                break;
            case 'mixed':
                return true;
                break;
            case 'bool':
                if (is_bool($value)) {
                    return true;
                }
                break;
            case 'null':
                if (is_null($value)) {
                    return true;
                }
                break;
            case 'json':
                if (is_array($value) || is_object($value)) {
                    return true;
                } else {
                    $test_encoding = json_encode($value);
                    return json_last_error() == \JSON_ERROR_NONE;
                }
                break;
            case 'datetime':
            case 'date':
                if ($value instanceof \DateTime) {
                    return true;
                } else {
                    try {
                        (new \DateTime($value));
                        return true;
                    } catch (\Exception $e) {
                        return false;
                    }
                }
                break;
            default:
                if ($value instanceof $type) {
                    return true;
                }
                break;
        }
        return false;
    }

}
