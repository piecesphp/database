<?php

/**
 * ExtensibleORM.php
 */
namespace PiecesPHP\Core\Database;

use PiecesPHP\Core\Database\Meta\MetaProperty;
use PiecesPHP\Core\Database\ORM\ActiveRecord;
use PiecesPHP\Core\Database\ORM\ORM;

/**
 * ExtensibleORM.
 *
 * @package     PiecesPHP\Core\Database
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2020
 */
class ExtensibleORM extends ORM
{

    /**
     * @var string $metaColumnName
     */
    protected $metaColumnName = 'meta';

    /**
     * @var MetaProperty[] $metaProperties
     */
    private $metaProperties = [];

    /**
     * ID insertado al hacer el último save
     *
     * @var int|null
     */
    protected $insertIdOnSave = null;

    /**
     * @param int $findValue
     * @param string $column
     * @param Database $database
     */
    public function __construct(int $findValue = null, string $column = 'primary_key', Database $database = null)
    {
        parent::__construct($findValue, $column, $database);

        $metaColumnName = $this->metaColumnName;
        $metaColumnValue = is_array($this->$metaColumnName) || $this->$metaColumnName instanceof \stdClass ? (object) $this->$metaColumnName : null;

        if (!is_null($metaColumnValue)) {

            foreach ($this->metaProperties as $name => $property) {

                if (isset($metaColumnValue->$name)) {

                    $this->getMetaProperty($name)->setValue($metaColumnValue->$name);

                }

            }

        }

    }

    /**
     * @param MetaProperty $property
     * @param string $name
     * @return static
     * @throws \Exception
     */
    public function addMetaProperty(MetaProperty $property, string $name)
    {

        if ($this->hasMetaProperty($name)) {
            throw new \Exception('Ya existe una propiedad con ese nombre: ' . $name);
        }

        $this->metaProperties[$name] = $property;

        return $this;

    }

    /**
     * @param string $name
     * @return MetaProperty
     * @throws \Exception
     */
    public function getMetaProperty(string $name)
    {

        if (!$this->hasMetaProperty($name)) {
            throw new \Exception('No existe una propiedad con ese nombre: ' . $name);
        }

        return $this->metaProperties[$name];

    }

    /**
     * @return MetaProperty[]
     */
    public function getMetaProperties()
    {

        return $this->metaProperties;

    }

    /**
     * @return bool
     */
    public function hasMetaProperty(string $name)
    {

        return array_key_exists($name, $this->metaProperties);

    }

    /**
     * __set
     *
     * @param string $name
     * @param mixed $value
     * @return void
     */
    public function __set($name, $value)
    {

        if (is_string($name) && $this->hasMetaProperty($name) && !array_key_exists($name, $this->fields->getNames())) {

            $this->getMetaProperty($name)->setValue($value);

        } else {

            parent::__set($name, $value);

        }

    }

    /**
     * __get
     *
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {

        if (is_string($name) && $this->hasMetaProperty($name) && !array_key_exists($name, $this->fields->getNames())) {

            return $this->getMetaProperty($name)->getValue();

        } else {

            return parent::__get($name);

        }

    }

    /**
     * @inheritDoc
     */
    public function save()
    {
        $metaColumnName = $this->metaColumnName;
        $metaColumnValue = is_array($this->$metaColumnName) || $this->$metaColumnName instanceof \stdClass ? (object) $this->$metaColumnName : new \stdClass();

        foreach ($this->metaProperties as $name => $property) {

            $value = $property->getValueToSQL();
            $metaColumnValue->$name = $value;

        }

        $this->$metaColumnName = $metaColumnValue;

        $saveResult = parent::save();

        if ($saveResult) {
            $this->insertIdOnSave = $this->getLastInsertID();
        }

        return $saveResult;

    }

    /**
     * @inheritDoc
     */
    public function update()
    {
        $metaColumnName = $this->metaColumnName;
        $metaColumnValue = is_array($this->$metaColumnName) || $this->$metaColumnName instanceof \stdClass ? (object) $this->$metaColumnName : new \stdClass();

        foreach ($this->metaProperties as $name => $property) {

            $value = $property->getValueToSQL();
            $metaColumnValue->$name = $value;

        }

        $this->$metaColumnName = $metaColumnValue;

        return parent::update();

    }

    /**
     * @return int|null Devuelve el ID insertado al hacer el último save o NULL si no ha sido guardado nada aún
     */
    public function getInsertIDOnSave()
    {
        return $this->insertIdOnSave;
    }

    /**
     * @inheritDoc
     */
    public function humanReadable()
    {
        $data = parent::humanReadable();

        foreach ($this->metaProperties as $name => $property) {
            $data['META:' . $name] = $property->getValueHuman();
        }

        $fields = $this->getFields();

        foreach ($fields as $name) {

            $value = $this->$name;

            if (is_subclass_of($value, ORM::class)) {

                $data[$name] = $this->recursiveHumanization($value);

            }

        }

        return $data;
    }

    /**
     * @param ORM $mapper
     * @return array
     */
    private function recursiveHumanization(ORM $mapper)
    {

        $dataHuman = $mapper->humanReadable();

        $subFields = $mapper->getFields();

        foreach ($subFields as $nameSubField) {

            if (is_subclass_of($mapper->$nameSubField, ORM::class)) {

                $dataHuman[$nameSubField] = $this->recursiveHumanization($mapper->$nameSubField);

            }

        }

        return $dataHuman;
    }

    /**
     * @return ActiveRecord
     */
    public static function model()
    {
        return (new static )->getModel();
    }
}
