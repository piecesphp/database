<?php

/**
 * SchemeCreator.php
 */
namespace PiecesPHP\Core\Database;

use JsonSerializable;
use ReflectionClass;

/**
 * SchemeCreator - Creador de entidades SQL a través de mapeadores
 *
 * @package     PiecesPHP\Core
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2019
 */
class SchemeCreator implements JsonSerializable
{

    /**
     * @var array Equivalencias de tipos de EntityMapper y SQL
     */
    protected $typeEquivalences = [
        'varchar' => 'varchar',
        'text' => 'text',
        'mediumtext' => 'mediumtext',
        'longtext' => 'longtext',
        'int' => 'int',
        'bigint' => 'bigint',
        'float' => 'double',
        'double' => 'double',
        'json' => 'longtext',
        'datetime' => 'datetime',
        'date' => 'date',
        'serialized_object' => 'longtext',
    ];

    /**
     * @var string[]
     */
    protected $typesCollations = [
        'varchar',
        'text',
        'mediumtext',
        'longtext',
    ];

    /**
     * @var array
     */
    protected $typesLengths = [
        'varchar' => 255,
    ];

    /**
     * @var ActiveRecordModel El modelo
     */
    private $model = null;

    /**
     * @var EntityMapper El mapeador
     */
    private $mapper = null;

    /**
     * @var ReflectionClass Clase reflexiva del mapeador
     */
    private $reflection = null;

    /**
     * @var string[]
     */
    private $sqlParts = [];

    /**
     * @param EntityMapper $mapper
     * @param bool $primaryKeyAlwaysAutoIncrement
     * @return static
     */
    public function __construct(EntityMapper $mapper, bool $primaryKeyAlwaysAutoIncrement = true)
    {
        $this->reflection = new ReflectionClass(get_class($mapper));
        $this->mapper = $mapper;
        $this->model = $mapper->getModel();

        $fields = $this->reflection->getProperty('fields');
        $fields->setAccessible(true);
        $fields = $fields->getValue($this->mapper);

        $table = $this->model->getTable();

        $keysSQL = [];
        $fieldsSQL = [];

        $this->sqlParts[] = "CREATE TABLE IF NOT EXISTS `{$table}`(";

        $i = 0;

        foreach ($fields as $field => $configurations) {

            $type = $configurations['type'];
            $length = $configurations['length'];
            $null = $configurations['null'];
            $primary_key = $configurations['primary_key'];
            $auto_increment = $configurations['auto_increment'];
            $reference_table = $configurations['reference_table'];
            $reference_field = $configurations['reference_field'];
            $meta = $configurations['meta'];

            if ($primary_key === true && $primaryKeyAlwaysAutoIncrement) {

                $auto_increment = true;

            }

            if (!$meta) {

                $fieldsSQL[$i] = [];

                $fieldsSQL[$i][] = "`{$field}`";

                $type = array_key_exists($type, $this->typeEquivalences) ? $this->typeEquivalences[$type] : $type;

                if (is_null($length) || $length < 1) {

                    $length = array_key_exists($type, $this->typesLengths) ? $this->typesLengths[$type] : null;

                }

                if (!is_null($length)) {

                    $fieldsSQL[$i][] = "{$type}($length)";

                } else {

                    $fieldsSQL[$i][] = "{$type}";

                }

                $withCollate = array_key_exists($type, $this->typesCollations);

                if ($null !== true) {

                    if ($withCollate) {

                        $fieldsSQL[$i][] = "COLLATE utf8_bin NOT NULL";

                    } else {

                        $fieldsSQL[$i][] = "NOT NULL";

                    }

                } else {

                    if ($withCollate) {

                        $fieldsSQL[$i][] = "COLLATE utf8_bin NOT NULL";

                    }

                }

                if ($auto_increment === true) {

                    $fieldsSQL[$i][] = "AUTO_INCREMENT";

                }

                if ($primary_key === true) {

                    $keysSQL[] = "\tPRIMARY KEY (`{$field}`)";

                }

                if (is_string($reference_table) && is_string($reference_field)) {

                    $keysSQL[] = "\tFOREIGN KEY (`{$field}`) REFERENCES {$reference_table}(`{$reference_field}`)";

                }

                $fieldsSQL[$i] = "\r\n\t" . trim(implode(' ', $fieldsSQL[$i]));

                $i++;

            }

        }

        $this->sqlParts[] = implode(", ", $fieldsSQL) . ',';
        $this->sqlParts[] = "\r\n" . implode(", \r\n", $keysSQL);
        $this->sqlParts[] = "\r\n) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;";

    }

    /**
     * @return bool
     */
    public function create()
    {

        $pdo = $this->model->prepare($this->getSQL());
        return $pdo->execute();

    }

    /**
     * @return string
     */
    public function getSQL()
    {

        return implode(' ', $this->sqlParts);

    }

    /**
     * @return array
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
        return [
            'mapper' => $this->mapper->humanReadable(),
            'sql' => $this->getSQL(),
        ];
    }

}
