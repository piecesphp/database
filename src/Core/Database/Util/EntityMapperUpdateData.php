<?php

/**
 * EntityMapperUpdateData.php
 */

namespace PiecesPHP\Core\Database\Util;

/**
 * EntityMapperUpdateData
 *
 * @package     PiecesPHP\Core\Database\Util
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2022
 */
class EntityMapperUpdateData
{
    /** @var array */
    protected $data = [];
    /** @var array */
    protected $where = [];

    /**
     * @param array $data
     * @param array $where
     */
    public function __construct(array $data, array $where)
    {
        $this->data = $data;
        $this->where = $where;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return array
     */
    public function getWhere()
    {
        return $this->where;
    }
}
