<?php

/**
 * ArrayObjectExtend.php
 */

namespace PiecesPHP\Core\Database\Util;

use ArrayIterator;
use ArrayObject;
use Countable;
use IteratorAggregate;
use Traversable;

/**
 * ArrayObjectExtend
 *
 * @package     PiecesPHP\Core\Database\Util
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2022
 */
class ArrayObjectExtend implements Countable, IteratorAggregate
{
    /**
     * @var ArrayObject
     */
    private $content = null;

    const STD_PROP_LIST = ArrayObject::STD_PROP_LIST;
    const ARRAY_AS_PROPS = ArrayObject::ARRAY_AS_PROPS;

    /**
     * @param array $array
     * @param int $flags
     * @param string|null $iteratorClass
     */
    public function __construct($array = [], int $flags = 0, string $iteratorClass = null)
    {
        $this->content = new ArrayObject($array, $flags, is_null($iteratorClass) ? ArrayIterator::class : $iteratorClass);
    }

    /**
     * @param mixed $value
     * @return void
     */
    public function append(mixed $value)
    {
        $this->content->append($value);
    }

    /**
     * @param int $flags
     * @return void
     */
    public function asort(int $flags = \SORT_REGULAR)
    {
        $this->content->asort($flags);
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return $this->content->count();
    }

    /**
     * @param array|object $array
     * @return array
     */
    public function exchangeArray($array)
    {
        return $this->content->exchangeArray($array);
    }

    /**
     * @return array
     */
    public function getArrayCopy()
    {
        return $this->content->getArrayCopy();
    }

    /**
     * @return int
     */
    public function getFlags()
    {
        return $this->content->getFlags();
    }

    /**
     * @return \Iterator
     */
    public function getIterator(): Traversable
    {
        return $this->content->getIterator();
    }

    /**
     * @return string
     */
    public function getIteratorClass()
    {
        return $this->content->getIteratorClass();
    }

    /**
     * @param int $flags
     * @return void
     */
    public function ksort(int $flags = SORT_REGULAR)
    {
        $this->content->ksort($flags);
    }

    /**
     * @return void
     */
    public function natcasesort()
    {
        $this->content->natcasesort();
    }

    /**
     * @return void
     */
    public function natsort()
    {
        $this->content->natsort();
    }

    /**
     * @param mixed $key
     * @return bool
     */
    public function offsetExists($key)
    {
        return $this->content->offsetExists($key);
    }

    /**
     * @param mixed $key
     * @return mixed
     */
    public function offsetGet($key)
    {
        return $this->content->offsetGet($key);
    }

    /**
     * @param mixed $key
     * @param mixed $value
     * @return void
     */
    public function offsetSet($key, $value)
    {
        $this->content->offsetSet($key, $value);
    }

    /**
     * @param mixed $key
     * @return void
     */
    public function offsetUnset($key)
    {
        $this->content->offsetUnset($key);
    }

    /**
     * @return string
     */
    public function serialize()
    {
        return $this->content->serialize();
    }

    /**
     * @param int $flags
     * @return void
     */
    public function setFlags(int $flags)
    {
        $this->content->setFlags($flags);
    }

    /**
     * @param string $iteratorClass
     * @return void
     */
    public function setIteratorClass(string $iteratorClass)
    {
        $this->content->setIteratorClass($iteratorClass);
    }

    /**
     * @param callable $callback
     * @return void
     */
    public function uasort(callable $callback)
    {
        $this->content->uasort($callback);
    }

    /**
     * @param callable $callback
     * @return void
     */
    public function uksort(callable $callback)
    {
        $this->content->uksort($callback);
    }

    /**
     * @param string $data
     * @return void
     */
    public function unserialize(string $data)
    {
        $this->content->unserialize($data);
    }
}
