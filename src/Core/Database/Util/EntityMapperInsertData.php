<?php

/**
 * EntityMapperInsertData.php
 */

namespace PiecesPHP\Core\Database\Util;

/**
 * EntityMapperInsertData
 *
 * @package     PiecesPHP\Core\Database\Util
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2022
 */
class EntityMapperInsertData
{
    /** @var array */
    protected $data = [];

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }
}
