<?php

/**
 * RequiredOptionMissedException.php
 */

namespace PiecesPHP\Core\Database\Exceptions;

use PiecesPHP\Core\Database\Enums\CodeStringExceptionsEnum;

/**
 * RequiredOptionMissedException - Excepción para cuando falta una opción obligatoria
 *
 * @package     PiecesPHP\Core\Database\Exceptions
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2018
 */
class RequiredOptionMissedException extends DatabaseClassesExceptions
{
    /**
     * __construct
     *
     * @param string $optionName
     * @return static
     */
    public function __construct(string $optionName)
    {
        parent::__construct(
            "La opción $optionName es requerida.",
            CodeStringExceptionsEnum::RequiredOptionMissed
        );
    }

}
