<?php

/**
 * Database.php
 */
namespace PiecesPHP\Core\Database;

/**
 * Database
 *
 * @package     PiecesPHP\Core\Database
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2020
 */
class Database extends \PDO implements \JsonSerializable

{
    /**
     * Configuración de $options en PDO por defecto
     */
    const DEFAULT_PDO_OPTIONS = [
        Database::ATTR_PERSISTENT => true,
    ];

    /**
     * @var string
     */
    private $database = '';
    /**
     * @var string|null
     */
    private $username = null;
    /**
     * @var string|null
     */
    private $password = null;
    /**
     * @var string|null
     */
    private $host = null;
    /**
     * @var string|null
     */
    private $driver = null;
    /**
     * @var \DateTime
     */
    private $createdAt = null;

    /**
     * @inheritDoc
     */
    public function __construct(string $dsn, string $username = null, string $password = null, array $options = [])
    {
        parent::__construct($dsn, $username, $password, $options);

        $pdoStatement = $this->query('SELECT DATABASE()');
        if ($pdoStatement !== false) {
            $database = $pdoStatement->fetchColumn();
            $this->database = is_string($database) ? $database : '';
        }
        $this->username = $username;
        $this->password = $password;
        $pdoStatement = $this->query('SELECT USER()');
        if ($pdoStatement !== false) {
            $host = $pdoStatement->fetchColumn();
            $this->host = is_string($host) && mb_strpos($host, '@') !== false ? explode('@', $host)[1] : '';
        }
        $this->driver = $this->getAttribute(self::ATTR_DRIVER_NAME);
        $this->createdAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getDatabaseName()
    {
        return $this->database;
    }

    /**
     * @return string|null
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string|null
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @return string
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * Conversión a string
     * @return string
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
        return [
            'database' => $this->database,
            'username' => $this->username,
            'password' => $this->password,
            'host' => $this->host,
            'driver' => $this->driver,
            'createdAt' => $this->createdAt,
        ];
    }

    /**
     * @param string $database
     * @param string $username
     * @param string $password
     * @param string $host
     * @param string $driver
     * @param string $charset
     * @param array $options
     * @return Database
     */
    public static function instance(string $database = ':memory:', string $username = 'admin', string $password = '', string $host = 'localhost', string $driver = 'sqlite', string $charset = 'utf8', array $options = [])
    {

        $instance = null;
        $dsn = "{$driver}:dbname={$database};host={$host}";

        $options = count($options) > 0 ? $options : self::DEFAULT_PDO_OPTIONS;

        if ($driver == 'sqlite') {
            $instance = new Database("$driver:$database", null, null, $options);
        } else {
            $instance = new Database($dsn, $username, $password, $options);
        }

        $instance->setAttribute(Database::ATTR_ERRMODE, Database::ERRMODE_EXCEPTION);

        if ($driver != 'sqlite') {
            $timezone = date('P');
            $prepareStatement = $instance->prepare("SET character set {$charset}; SET time_zone='{$timezone}';");
            $prepareStatement->execute();
            $prepareStatement->closeCursor();
        }

        return $instance;

    }

    /**
     * @param string $database
     * @param string $username
     * @param string $password
     * @param string $host
     * @param string $charset
     * @param array $options
     * @return Database
     */
    public static function instanceMySQL(string $database, string $username = 'admin', string $password = '', string $host = 'localhost', string $charset = 'utf8', array $options = [])
    {
        return self::instance($database, $username, $password, $host, 'mysql', $charset, $options);
    }

}
